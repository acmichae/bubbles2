#-----------------------------------------------------------------------------------------------------------------------------------
# 
# Defines the following CMake variables:
# 
#  PROJECT_DIR 				absolute path to root of the project (the one with ext, src, data and so on)
#  PLATFORM 				{macosx} (also win32, win64, linux_i386 and linux_amd64, but those are not yet in use)
# 
# 
# Defines the following MACROS:
# 
#  USE_BOOST(EXECUTABLE)
#  USE_DLIB(EXECUTABLE)
#  USE_OPENCV(EXECUTABLE)
# 
#------------------------------------------------------------------------------------------------------------------------------------



## Detect platform

if(MSVC)
	if(CMAKE_SIZEOF_VOID_P EQUAL 4)
		set(PLATFORM "win32")
		#message(FATAL_ERROR "CMakeLists has not been adapted for win32")
	elseif(CMAKE_SIZEOF_VOID_P EQUAL 8)
		add_definitions(-D_WIN64)
		set(PLATFORM "win64")
		#message(FATAL_ERROR "CMakeLists has not been adapted for win64")
	else()
		message(FATAL_ERROR "Unrecognized architecture!")
	endif()
elseif(UNIX)
	if(APPLE)
		set(PLATFORM "macosx")
	else()
		if(CMAKE_SIZEOF_VOID_P EQUAL 4)
			set(PLATFORM "linux_i386")
			message(FATAL_ERROR "CMakeLists has not been adapted for linux_i386")
		elseif(CMAKE_SIZEOF_VOID_P EQUAL 8)
			set(PLATFORM "linux_amd64")
			message(FATAL_ERROR "CMakeLists has not been adapted for linux_amd64")
		else()
			message(FATAL_ERROR "Unrecognized architecture!")
		endif()
	endif()
else()
	message(FATAL_ERROR "Unsupported architecture!")
endif()




## General setup

set(PROJECT_DIR_REL "${CMAKE_CURRENT_LIST_DIR}/..")
get_filename_component(PROJECT_DIR "${PROJECT_DIR_REL}" ABSOLUTE)

set(EXT_COMPILED_DIR "${PROJECT_DIR}/ext/compiled")

# Output directories

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG "${PROJECT_DIR}/prod")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE "${PROJECT_DIR}/prod")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${PROJECT_DIR}/prod")

set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_DEBUG "${PROJECT_DIR}/prod")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_RELEASE "${PROJECT_DIR}/prod")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${PROJECT_DIR}/prod")


# Compiler settings

if(PLATFORM STREQUAL "win32" OR PLATFORM STREQUAL "win64")
	
	set(CMAKE_GENERATOR_TOOLSET "Intel C++ Compiler XE 14.0" CACHE STRING "Platform Toolset" FORCE) 	

endif()
# set(CMAKE_C_COMPILER /usr/bin/icc CACHE STRING "" FORCE)
# set(CMAKE_CXX_COMPILER /usr/bin/icpc CACHE STRING "" FORCE)




## Project name

macro(try_to_set_project_name NAME)
	if(NOT DEFINED PROJECT_NAME)
		set(PROJECT_NAME "${NAME}")
		project("${PROJECT_NAME}")
	endif()
endmacro()




## Third party libraries

# Utility macros

macro(download_and_extract_archive URL ARCHIVE_FILENAME WORKING_DIR)
	set(ARCHIVE_FULL_PATH "${WORKING_DIR}/${ARCHIVE_FILENAME}")
	message(STATUS "Downloading ${ARCHIVE_FILENAME} to ${WORKING_DIR}")
	file(DOWNLOAD "${URL}" ${ARCHIVE_FULL_PATH})
	message(STATUS "Extracting ${ARCHIVE_FILENAME}")
	execute_process(COMMAND ${CMAKE_COMMAND} -E tar xz "${ARCHIVE_FULL_PATH}" WORKING_DIRECTORY "${WORKING_DIR}")
endmacro()


macro(git_clone URL FOLDER_NAME WORKING_DIRECTORY)
	if(NOT IT_FOUND)
		find_package(Git)
	endif()
	if(GIT_FOUND)
		execute_process(COMMAND ${GIT_EXECUTABLE} clone "${URL}" "${WORKING_DIRECTORY}/${FOLDER_NAME}")
	else()
		message(WARNING "Could not use git clone for ${URL} since git was not found.")
	endif()
endmacro()



# OpenCV

macro(use_opencv EXECUTABLE)
	if(PLATFORM STREQUAL "macosx" OR PLATFORM STREQUAL "win32" OR PLATFORM STREQUAL "win64")
		set(OpenCV_DIR "${EXT_COMPILED_DIR}/OpenCV-3.0.0-rc1_gcc_Release_Static_IPP_Python2/share/OpenCV" CACHE STRING "" FORCE)
	endif()
	find_package(OpenCV REQUIRED)
	include_directories("${OpenCV_INCLUDE_DIRS}")
	target_link_libraries("${EXECUTABLE}" ${OpenCV_LIBS})
endmacro()



# Boost

macro(use_boost EXECUTABLE)	
	if(PLATFORM STREQUAL "macosx" OR PLATFORM STREQUAL "win32" OR PLATFORM STREQUAL "win64")
		set(BOOST_ROOT "${EXT_COMPILED_DIR}/Boost_1_57_0_IntelCompiler_Release_Static_MultiThreading" CACHE STRING "" FORCE)
	endif()
	find_package(Boost COMPONENTS system filesystem python serialization program_options random REQUIRED)
	include_directories("${Boost_INCLUDE_DIR}")
	target_link_libraries("${EXECUTABLE}" ${Boost_LIBRARIES})
endmacro()



# Dlib

macro(use_dlib EXECUTABLE)
	include("${PROJECT_DIR}/ext/src/cross_platform/dlib/dlib-18.15/dlib/cmake")
	target_link_libraries("${EXECUTABLE}" dlib)
endmacro()


macro(use_dlib_most_recent EXECUTABLE)
	include("${PROJECT_DIR}/ext/src/cross_platform/dlib/dlib_most_recent/dlib/cmake")
	target_link_libraries("${EXECUTABLE}" dlib)
endmacro()



# Shark 

macro(use_shark EXECUTABLE)
	if(PLATFORM STREQUAL "macosx")
		set(Shark_DIR "${EXT_COMPILED_DIR}/Shark_most_recent/lib/CMake/Shark" CACHE STRING "" FORCE)
	endif()
	find_package(Shark)
	include_directories("${SHARK_INCLUDE_DIRS}")
	target_link_libraries("${EXECUTABLE}" "${SHARK_LIBRARIES}")	
endmacro()

