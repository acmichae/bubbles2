import cv2
import skimage
import skimage.feature
import matplotlib.pyplot as plt

# General introduction:
# https://chrisjmccormick.wordpress.com/2013/05/09/hog-person-detector-tutorial/

filename = '../../data/testImages/general/Lena64x64.jpg'


image = cv2.imread(filename, cv2.CV_LOAD_IMAGE_GRAYSCALE)

image_size = image.shape[::-1] 		# so we get it as (width, height)


# http://bbabenko.tumblr.com/post/56701676646/when-hogs-py
orientations = 9
pixels_per_cell = (8, 8)
cells_per_block = (2, 2)

skimage_hog, skimage_hog_image = skimage.feature.hog(image, orientations=orientations, pixels_per_cell=pixels_per_cell, cells_per_block=cells_per_block, visualise=True)
print "length of skimage hog: %d" % len(skimage_hog)


skimage_hog_image_rescaled = skimage.exposure.rescale_intensity(skimage_hog_image)
plt.imshow(skimage_hog_image_rescaled, cmap=plt.cm.gray)
plt.show()


# http://docs.opencv.org/modules/gpu/doc/object_detection.html#gpu-hogdescriptor-hogdescriptor:
#
# (for the gpu variant)
# win_size - Detection window size. Align to block size and block stride.
# block_size - Block size in pixels. Align to cell size. Only (16,16) is supported for now.
# block_stride - Block stride. It must be a multiple of cell size.
# cell_size - Cell size. Only (8, 8) is supported for now.
# nbins - Number of bins. Only 9 bins per cell are supported for now.
#




win_size = image_size
block_size = tuple([a*b for a, b in zip(cells_per_block, pixels_per_cell)])
block_stride = pixels_per_cell
cell_size = pixels_per_cell
nbins = orientations


# http://www.juergenwiki.de/work/wiki/doku.php?id=public:hog_descriptor_computation_and_visualization:
#
# winSize, typically 64x128 (width x height)
# blockSize, typically 16 pixels side length
# blockStepSize (blockStride), typically 8 pixels shift size
# cellSize, typically 8 pixels side length
# nr of gradient bins per cell, typically 9 bins
#
#  HOG descriptor length = #Blocks 				* #CellsPerBlock 	* #BinsPerCell
#                       = (64/8-1) * (128/8-1) 	* (2*2) 			* 9
#                       = 7        * 15        	* 4    				* 9
#                       = 3780

print 'winsize: ' + str(win_size)
print 'block_size: ' + str(block_size)
print 'block_stride: ' + str(block_stride)
print 'cell_size: ' + str(cell_size)
print 'nbins: ' + str(nbins)


opencv_hog = cv2.HOGDescriptor(win_size, block_size, block_stride, cell_size, nbins)


opencv_hog_result = opencv_hog.compute(image)
print "length of opencv hog: %d" % len(opencv_hog_result) 






