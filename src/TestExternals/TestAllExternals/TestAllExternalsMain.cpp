#include <iostream>
#include <sstream>

#include <boost/version.hpp>
#include <dlib/revision.h>
#include <opencv2/opencv.hpp>
#include <shark/Core/Shark.h>



using namespace std;

int main(){
    
    // Boost
    cout << "Boost version: " << BOOST_LIB_VERSION << endl;
    
        
    //Dlib
    cout << "dlib version: " << DLIB_MAJOR_VERSION << "." << DLIB_MINOR_VERSION << endl;
    
    
    //OpenCV
    cout << "OpenCV version: " << CV_VERSION << endl;
    
    // Shark
    stringstream s;
    shark::Shark::version_type sharkVersion;
    cout << "Shark version: " << sharkVersion.MAJOR() << "." << sharkVersion.MINOR() << "." << sharkVersion.PATCH() << endl;
    
    return 0;
    
}