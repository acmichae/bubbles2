#include <iostream>
#include <sstream>

#include <shark/Core/Shark.h>



using namespace std;

int main(){
    stringstream s;
    shark::Shark::version_type sharkVersion;
    cout << "Shark library version: " << sharkVersion.MAJOR() << "." << sharkVersion.MINOR() << "." << sharkVersion.PATCH() << endl;
    
    return 0;
    
}