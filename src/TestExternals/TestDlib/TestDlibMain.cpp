#include <iostream>

#include <dlib/revision.h>

using namespace std;

int main(){
    
    cout << "dlib version: " << DLIB_MAJOR_VERSION << "." << DLIB_MINOR_VERSION << endl;
    
    return 0;
    
}