#include <iostream>

#include <boost/chrono.hpp>

#include <opencv2/opencv.hpp>

#include "../DropletSorter/Utilities.hpp"

using std::cout;
using std::endl;
using std::vector;
using std::to_string;

using std::chrono::steady_clock;
using std::chrono::microseconds;
using std::chrono::nanoseconds;
using std::chrono::duration_cast;

using namespace cv;

using namespace NonMaximumSuppression;


bool firstCircleIsLeft(Vec3f left, Vec3f right){
    
    return left[0] < left[1];
}

Rect softCropRoi(Size image, Rect roi){
    
    return Rect(Point(0, 0), image) & roi;
    
}


inline vector<Vec3f> recognizeBubbles(Mat frame){
    
    
    Mat processedFrame;
    
    cvtColor(frame, processedFrame, CV_BGR2GRAY);
    
    
    vector<Vec3f> circles;
    circles.reserve(20);
    
    /// Apply the Hough Transform to find the circles
    
    int dp = 1;                                                                                                              ;
    
    int canny_threshold = 100;
    int acc_threshold = 30;
    
    int min_radius = 20;
    int max_radius = 40;
    
    int min_dist = 2*min_radius+10;
    
    HoughCircles(processedFrame, circles, CV_HOUGH_GRADIENT, dp, min_dist, canny_threshold, acc_threshold, min_radius, max_radius);
    
    return circles;
    
}



int main(){
    
    VideoCapture capture("/Users/michael/Documents/ETH/Master/SemesterProject/bubbles2/data/testVideos/5cmStraightA02__001.avi");
    
    Size frameSize;
    frameSize.width = capture.get(CAP_PROP_FRAME_WIDTH);
    frameSize.height = capture.get(CAP_PROP_FRAME_HEIGHT);
    
    int nFrames = capture.get(CAP_PROP_FRAME_COUNT);
    
    
    Mat currentFrameRaw, currentFrame, grayFrame;
    Rect roi(500, 95, 300, 100);
    
    Mat prevImg, nextImg;
    vector<Point2f> prevPts, nextPts;
    
    
    
    vector<Vec3f> previousBubbles;
    vector<int> previousUniqueBubbleIdentifiers;
    
    
    FileStorage fs("/Users/michael/Documents/ETH/Master/SemesterProject/bubbles2/data/dropletSorting/dataSet13/hogDescriptor.yml", FileStorage::READ);
    FileNode fn(fs["hogDescriptor"]);
    
    HOGDescriptor hogDescriptor;
    hogDescriptor.read(fn);
    
    
    
    int uniqueBubbleIdentifier = 0;
    
    vector<Mat> frames;
    
    for(int i = 0; i < nFrames; i++){
        
        capture >> currentFrame;
        
        if(currentFrame.empty()){
            
            cout << "frame empty, continue" << endl;
            continue;
        }
        
        
        frames.push_back(currentFrame.clone());
        
    }
    
    auto startAll = steady_clock::now();
    
    vector<int> currentUniqueBubbleIdentifiers(100);
    //cout << " capacity: " << currentUniqueBubbleIdentifiers.capacity() << endl;
    
    vector<Rect> foundRects(100);
    vector<Point> foundLocationsPoint(100);
    vector<double> weights(100);
    
    for(int i = 0; i < frames.size(); i++){
    
        auto start = steady_clock::now();
        
        
        
        currentFrame = frames[i];
        
        
        
        //cvtColor(currentFrame, grayFrame, COLOR_BGR2GRAY);
        
        //Mat display = currentFrame.clone();

       
        auto preRecognizeBubbles = steady_clock::now();

        
        vector<Vec3f> currentBubbles = recognizeBubbles(currentFrame(roi));
        currentUniqueBubbleIdentifiers.resize(0);

        
        
        auto postRecognizeBubbles = steady_clock::now();
        
        
        
        /*
        for(int j = 0; j < previousUniqueBubbleIdentifiers.size(); j++){
            
            //cout << "prev unique ids: " << endl;
            cout << previousUniqueBubbleIdentifiers[j] << endl;
            
        }
        
        */
        
        
        std::sort(currentBubbles.begin(), currentBubbles.end(), firstCircleIsLeft);
        
        
        //cout << "Found bubbles in current frame: " << endl;
        
        auto preMatchingBubbles = steady_clock::now();
        
        for(int j = 0; j < currentBubbles.size(); j++){
            
            
            
            int previousX = currentBubbles[j][0] - 25;
            int slack = 10;
            
            int matchedInPreviousBubbles = -1;
            
            for(int k = 0; k < previousBubbles.size(); k++){
                
                //cout << "Trying to find a bubble with x between " << previousX - slack << " and " << previousX + slack << endl;
                //cout << "The bubble we're lookking at: " << previousBubbles[k] << endl;
                
                if(previousBubbles[k][0] >= previousX - slack && previousBubbles[k][0] <= previousX + slack){
                    
                    //cout << "current bubble " << currentBubbles[j] << " had an earlier life as " << previousBubbles[k] << endl;
                    matchedInPreviousBubbles = k;
                    break;
                }
                
            }
            
            if(matchedInPreviousBubbles != -1){
                
                // cout << "current bubble: " << currentBubbles[j] << " had an earlier life as " << previousBubbles[matchedInPreviousBubbles] << endl;
                // cout << "k: " << matchedInPreviousBubbles << endl;
                currentUniqueBubbleIdentifiers.push_back(previousUniqueBubbleIdentifiers[matchedInPreviousBubbles]);
                
            }else{
                
                // cout << "Found new bubble: " << currentBubbles[j] << ", assigning it " << uniqueBubbleIdentifier  << endl;
                currentUniqueBubbleIdentifiers.push_back(uniqueBubbleIdentifier++);
                
            }
            
            
            //cout << currentBubbles[j] << endl;
            
        }
        
        
        auto postMatchingBubbles = steady_clock::now();
        
        
        
       
        
        auto preRecognizingCells = steady_clock::now();
        
        //cout << "Found " << currentBubbles.size() << " bubbles" << endl;
        
        for(int j = 0; j < currentBubbles.size(); j++){
            
            
            auto prePrep = steady_clock::now();
            Vec3f currentBubble = currentBubbles[j];
            
            Point center(currentBubble[0], currentBubble[1]);
            int radius = currentBubble[2];
            
            Point topRight = center - Point(radius+5, radius+5);
            
            
            Rect bubbleRoi = Rect(topRight, Size(2*radius+10, 2*radius+10));
            
            //circle(display(roi), center, radius, Scalar(255, 255, 0), 1, LINE_AA);
            
            
            //Mat roiedDisplay = display(roi);

            

            Mat bubbleRoied = currentFrame(roi)(softCropRoi(roi.size(), bubbleRoi));
            // Mat bubbleRoiedDisplay = display(roi)(softCropRoi(roi.size(), bubbleRoi));
            
            
            auto postPrep = steady_clock::now();

            
            //rectangle(roiedDisplay, bubbleRoi, Scalar(255, 0, 255), 1, LINE_AA);
            
            
            foundLocationsPoint.resize(0);
            weights.resize(0);
            //imshow("c", bubbleRoied);
            
            auto preCompute = steady_clock::now();
            
            hogDescriptor.detect(bubbleRoied, foundLocationsPoint, weights);
            
            
            auto postCompute = steady_clock::now();
            
            /*
            cout << "Number of objects detected (before non-maxima suppression): " << endl;
            cout << foundLocationsPoint.size() << endl;
            */
             
            //Mat testImageUnpruned = bubbleRoied.clone();
            
            foundRects.resize(0);
            
            for(int i = 0; i < foundLocationsPoint.size(); i++){
                
                //rectangle(testImageUnpruned, foundLocationsPoint[i], foundLocationsPoint[i] + Point(20, 20), Scalar(0, 255, 0), 1, LINE_AA);
                foundRects.push_back(Rect(foundLocationsPoint[i], Size(20, 20)));
                
            }
            
            
            // imshow("Before Non Maximum Suppression", testImageUnpruned);
            
            //Mat testImagePruned = bubbleRoied.clone();
            
            auto prePruningCells = steady_clock::now();
            
            vector<Rect> prunedRects = pruneRects(foundRects, weights, 0.4);
            
            
            auto postPruningCells = steady_clock::now();
            
          
            
            
            //cout << "Bubble #" << currentUniqueBubbleIdentifiers[j] << " contains " << foundRects.size() << " cells" << endl;
            
            /*
             cout << "Number of objects detected (after non-maxima suppression): " << endl;
             cout << prunedRects.size() << endl;
            */
            
            //Mat b = display(roi)(softCropRoi(roi.size(), bubbleRoi));
            
            
            /*
            for(int i = 0; i < prunedRects.size(); i++){
                
                //rectangle(b, prunedRects[i], Scalar(0, 255, 0), 1, LINE_AA);
                // cout << foundRects[i] << endl;
            }
            
            */
            //putText(roiedDisplay, to_string(prunedRects.size()), bubbleRoi.tl(), 2, 1, Scalar(0, 0, 0));
            
            int hogComputation = duration_cast<microseconds>(postCompute - preCompute).count();
            cout << "hog detection took " << hogComputation << " microseconds" << endl;
            
            int pruningCells = duration_cast<microseconds>(postPruningCells - prePruningCells).count();
            cout << "pruning cells took " << pruningCells << " microseconds" << endl;
        }
        
        auto postRecognizingCells = steady_clock::now();
        
        
        
        previousBubbles = currentBubbles;
        previousUniqueBubbleIdentifiers = currentUniqueBubbleIdentifiers;
        
        
        // rectangle(display, roi, Scalar(0, 255, 255), 2, LINE_AA);
        
        auto end = steady_clock::now();
        
        
        
        //imshow("", display);
        //waitKey();
        
        cout << "Detected " << currentBubbles.size() << " bubbles" << endl;
        
        int recognizeBubbles = duration_cast<microseconds>(postRecognizeBubbles - preRecognizeBubbles).count();
        cout << "recognizing bubbles took " << recognizeBubbles << " microseconds" << endl;
        
        int matchingBubbles = duration_cast<microseconds>(postMatchingBubbles - preMatchingBubbles).count();
        cout << "matching bubbles took " << matchingBubbles << " microseconds" << endl;
        
        int recognizingCells = duration_cast<microseconds>(postRecognizingCells - preRecognizingCells).count();
        cout << "recognizing cells took " << recognizingCells << " microseconds" << endl;
        
        int totalFrame = duration_cast<microseconds>(end - start).count();
        cout << "one frame took " << totalFrame << " microseconds (" << double(totalFrame)/1000 << " ms)" << endl;
        cout << endl;
        
        
        
    }
    
    
    auto endAll = steady_clock::now();
    
    int totalAll = duration_cast<microseconds>(endAll - startAll).count();
    
    cout << "one frame took on average " << totalAll/frames.size() << " microseconds (" << double(totalAll)/frames.size()/1000 << " ms)" << endl;
    
}

