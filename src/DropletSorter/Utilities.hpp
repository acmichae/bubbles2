#include <iostream>
#include <chrono>
#include <iomanip>

#include <cassert>

#include <opencv2/opencv.hpp>

#include <boost/random.hpp>


using std::cout;
using std::endl;
using std::cerr;
using std::vector;
using std::string;

using std::setw;
using std::setfill;

using std::chrono::system_clock;
using std::chrono::steady_clock;
using std::ctime;
using std::chrono::hours;
using std::chrono::minutes;
using std::chrono::seconds;
using std::chrono::duration_cast;

using boost::random::uniform_int_distribution;
using boost::random::mt19937;

using namespace cv;
using namespace cv::ml;


namespace NonMaximumSuppression{
    
    float overlapRatio(Rect a, Rect b);
    
    Mat calculateOverlapMatrix(vector<Rect> input);
    
    Mat generateImage(vector<Rect> input);
    
    vector<vector<Rect> > calculateGroups(vector<Rect> input);
    
    vector<Rect> pruneRects(vector<Rect> input, vector<double> &weights, double overlapThreshold, bool verbose = false);
    
}

namespace miscUtilities{
    
    string nicelyFormatDuration(steady_clock::time_point start, steady_clock::time_point end);
    
}



namespace SVMUtilities{
    
    
    Mat convertHogFeatureVectorToMatForSvm(vector<float> hogFeatureVector);
    
    void appendVectorToFeatureMatrixForSvmTraining(Mat &featureMatrix, vector<float> hogFeatureVector);
    
    vector<float> computeSvmDetectorForHOGDescriptor(const Ptr<SVM>& svm);
    
    Mat generateMat(vector<vector<float> > input);
    
    
    vector<double> linspace(double min, double max, int n);
    
    // stolen from opencv_source/modules/ml/src/precomp.hpp
    void setRangeVector(std::vector<int>& vec, int n);
    
    
    struct ClassifierSetting{
        
        double C;
        double nu;
        
        ClassifierSetting(double C = 0, double nu = 0):C(C), nu(nu){};
        
    };
    
    vector<ClassifierSetting> cartesianGrid(vector<double> Cs, vector<double> nus);
    

    Ptr<SVM> gridSearchLinearNuSVR(vector<vector<float> > features, vector<float> responses, int kFold, vector<ClassifierSetting> classifierSettings, HOGDescriptor hogDescriptor, vector<Mat> images, vector<float> scores, int verbosity = 0);
    Ptr<SVM> gridSearchLinearNuSVR(vector<vector<float> > features, vector<float> responses, int kFold, vector<ClassifierSetting> classifierSettings, HOGDescriptor hogDescriptor, vector<Mat> images, int verbosity = 0);


    
    
}