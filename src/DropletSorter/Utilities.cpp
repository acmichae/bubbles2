#include <iostream>

#include <opencv2/opencv.hpp>


#include "Utilities.hpp"



using std::cout;
using std::endl;
using std::vector;
using std::to_string;
using std::runtime_error;

using namespace cv;
using namespace cv::ml;

namespace NonMaximumSuppression{
    
    
    float overlapRatio(Rect a, Rect b){
        
        Rect intersection = a & b;
        
        return float(intersection.area())/a.area();
        
    }
    
    Mat calculateOverlapMatrix(vector<Rect> input){
        
        Mat result(input.size(), input.size(), CV_32F);
        
        for(int i = 0; i < input.size(); i++){
            
            for(int j = 0; j < input.size(); j++){
                
                if(i != j){
                    result.at<float>(i, j) = overlapRatio(input[i], input[j]);
                }else{
                    result.at<float>(i, j) = 0;
                }
                
            }
            
        }
        
        return result;
    }
    
    
    
    
    Mat generateImage(vector<Rect> input){
        
        int maximalX = 0;
        int maximalY = 0;
        
        for(int i = 0; i < input.size(); i++){
            
            maximalX = max(maximalX, input[i].br().x);
            maximalY = max(maximalY, input[i].br().y);
            
        }
        
        
        Mat result(maximalX+50, maximalY+50, CV_32FC3);
        
        result = Scalar(255, 255, 255);
        
        for(int i = 0; i < input.size(); i++){
            
            rectangle(result, input[i], Scalar(255, 0, 0), 1, LINE_AA);
            
        }
        
        for(int i = 0; i < input.size(); i++){
            
            for(int j = i+1; j < input.size(); j++){
                
                rectangle(result, input[i] & input[j], Scalar(0, 255, 0), 1, LINE_AA);
                
            }
            
        }
        
        return result;
        
    }
    
    vector<vector<Rect> > calculateGroups(vector<Rect> input){
        
        vector<int> groupNumberOfRect;
        vector<vector<int> > rectGroups;
        
        // setting up
        for(int i = 0; i < input.size(); i++){
            groupNumberOfRect.push_back(i);
            rectGroups.push_back(vector<int>(i));
        }
        
        
        for(int i = 0; i < input.size(); i++){
            
            for(int j = i+1; j < input.size(); j++){
                
                if(overlapRatio(input[i], input[j]) > 0){
                    
                    for(int k = 0; k < input.size(); k++){
                        
                        if(groupNumberOfRect[k] == groupNumberOfRect[j]){
                            groupNumberOfRect[k] = groupNumberOfRect[i];
                        }
                    }
                    
                }
                
            }
            
        }
        
        
        vector<vector<Rect> > temp(input.size(), vector<Rect>());
        
        for(int i = 0; i < input.size(); i++){
            temp[groupNumberOfRect[i]].push_back(input[i]);
        }
        
        vector<vector<Rect> > result;
        
        for(int i = 0; i < input.size(); i++){
            if(temp[i].size() != 0){
                result.push_back(temp[i]);
            }
        }
        
        
        return result;
    }
    
    vector<Rect> pruneRects(vector<Rect> input, vector<double> &weights, double overlapThreshold, bool verbose){
        
        vector<Rect> result = input;
        
        int c = 0;
        while(1){
            
            c++;
            Mat overlapMatrix = calculateOverlapMatrix(result);
            
            if(verbose){
                cout << "Overlap matrix " << c << "th pass" << endl;
                cout << overlapMatrix << endl;
            }
            
            double maxValue;
            Point maxLoc;
            
            minMaxLoc(overlapMatrix, NULL, &maxValue, NULL, &maxLoc);
            
            
            if(maxValue < overlapThreshold)
                break;
            
            
            Rect a = result[maxLoc.x];
            Rect b = result[maxLoc.y];
            
            double weightA = weights[maxLoc.x];
            double weightB = weights[maxLoc.y];
            
            Rect newRect;
            double newWeight;
            
            if(false && a.size() == b.size()){
                
                int newCenterX = (a.x*weightA + b.x*weightB)/2;
                int newCenterY = (a.y*weightA + b.y*weightB)/2;
                
                Point newCenter(newCenterX, newCenterY);
                newWeight = (weightA + weightB)/2;
                
                newRect = Rect(newCenter, a.size());
                
                
            }else{
                
                
                // for lack of a better idea, just push the one with the higher score back into the list
                
                if(weightA >= weightB){
                    
                    newRect = a;
                    newWeight = weightA;
                    
                }else{
                    
                    newRect = b;
                    newWeight = weightB;
                    
                }
                
            }
            
            if(maxLoc.x > maxLoc.y){
                
                result.erase(result.begin() + maxLoc.x);
                result.erase(result.begin() + maxLoc.y);
                weights.erase(weights.begin() + maxLoc.x);
                weights.erase(weights.begin() + maxLoc.y);
                
            }else{
                
                result.erase(result.begin() + maxLoc.y);
                result.erase(result.begin() + maxLoc.x);
                weights.erase(weights.begin() + maxLoc.y);
                weights.erase(weights.begin() + maxLoc.x);
                
            }
            
            if(verbose){
                cout << "Due to overlap of " << maxValue << ", replacing :" << endl;
                cout << a << " and " << endl;
                cout << b << " wit  h " << endl;
                cout << newRect << endl;
            }
            result.push_back(newRect);
            weights.push_back(newWeight);
            
            
            
        }
        
        return result;
        
    }
    
    
}



namespace miscUtilities{
    
    string nicelyFormatDuration(steady_clock::time_point start, steady_clock::time_point end){
        
        int h = duration_cast<hours>(end - start).count();
        int m = duration_cast<minutes>(end - start).count() % 60;
        int s = duration_cast<seconds>(end - start).count() % 60;
        
        std::ostringstream result;
        
        if(h == 0 && m == 0 && s == 0){
            return "0 s";
        }
        
        
        if(h != 0){
            
            result << h << " h ";
        }
        
        if(m != 0){
            
            result << m << " min ";
        }
        
        if(s != 0){
            
            result << s << " s ";
        }
        
        
        return result.str();
        
    }
    
    
}



namespace SVMUtilities {
    
    Mat convertHogFeatureVectorToMatForSvm(vector<float> hogFeatureVector){
        
        Mat result(hogFeatureVector, true);
        
        int nChannels = 1, nRows = 1;
        return result.reshape(nChannels, nRows);        // This ensures a one row Mat (takes channels and rows, not columns and rows)
    }
    
    
    void appendVectorToFeatureMatrixForSvmTraining(Mat &featureMatrix, vector<float> hogFeatureVector){
        
        featureMatrix.push_back(convertHogFeatureVectorToMatForSvm(hogFeatureVector));
        
    }
    
    
    
    vector<float> computeSvmDetectorForHOGDescriptor(const Ptr<SVM>& svm)
    {
        
        /*
         Black magic, I don't really understand what's happening here.
         It somehow computes the coefficients needed by HogDetector::setSVMDetector()
         Copied from opencv-source/samples/cpp/train_HOG.cpp -> look for get_svm_detector()
         Also refer to
         https://github.com/DaHoC/trainHOG/blob/master/libsvm/libsvm.h
         and
         https://github.com/DaHoC/trainHOG/blob/master/svmlight/svmlight.h
         getSingleDetectingVector() does the same job
         See also:
         http://opencv-users.1802565.n2.nabble.com/training-a-HOG-descriptor-td6363437.html
         */
        
        
        vector<float> hog_detector;
        
        // get the support vectors
        Mat sv = svm->getSupportVectors();
        const int sv_total = sv.rows;
        
        
        // get the decision function
        Mat alpha, svidx;
        double rho = svm->getDecisionFunction(0, alpha, svidx);
        
        
        CV_Assert( alpha.total() == 1 && svidx.total() == 1 && sv_total == 1 );
        
        CV_Assert( (alpha.type() == CV_64F && alpha.at<double>(0) == 1.) ||
                  (alpha.type() == CV_32F && alpha.at<float>(0) == 1.f) );
        CV_Assert( sv.type() == CV_32F );
        
        hog_detector.clear();
        
        hog_detector.resize(sv.cols + 1);
        memcpy(&hog_detector[0], sv.ptr(), sv.cols*sizeof(hog_detector[0]));
        hog_detector[sv.cols] = (float)-rho;
        
        
        
        
        /*
         
         cout << "sv_total: " << sv_total << endl;
         
         cout << "rho" << endl;
         cout << rho << endl;
         
         cout << "alpha" << endl;
         cout << alpha << endl;
         
         cout << "svidx" << endl;
         cout << svidx << endl;
         
         cout << "output length: " << hog_detector.size() << endl;
         
         for(int i = 0; i < hog_detector.size(); ++i){
         
         cout << hog_detector[i] << endl;
         
         }
         
         */
        return hog_detector;
    }
    
    
    
    
    Mat generateMat(vector<vector<float> > input){
        
        
        int nRows;
        int nCols;
        
        if(input.size() == 0){
            
            nRows = 0;
            nCols = 0;
            
        }else if(input[0].size() == 0){
            
            nRows = 0;
            nCols = 0;
            
        }else{
            
            nRows = input.size();
            nCols = input[0].size();
        }
        
        
        Mat result(nRows, nCols, CV_32F);
        
        
        for(int row = 0; row < nRows; row++){
            
            if(input[row].size() != nCols){
                
                throw string("All rows have to have " + to_string(nCols) + " elements (we have " + to_string(input[row].size()));
                
            }
            
            for(int col = 0; col < nCols; col++){
                
                result.at<float>(row, col) = input[row][col];
                
            }
            
        }
        
        return result;
        
        
    }
    
    
    
    vector<double> linspace(double min, double max, int n)
    {
        vector<double> result;
        // vector iterator
        int iterator = 0;
        
        for (int i = 0; i <= n-2; i++)
        {
            double temp = min + i*(max-min)/(floor((double)n) - 1);
            result.insert(result.begin() + iterator, temp);
            iterator += 1;
        }
        
        //iterator += 1;
        
        result.insert(result.begin() + iterator, max);
        return result;
    }
    
    
    
    
    // stolen from opencv_source/modules/ml/src/precomp.hpp
    void setRangeVector(std::vector<int>& vec, int n)
    {
        vec.resize(n);
        for( int i = 0; i < n; i++ )
            vec[i] = i;
    }
    
    
    vector<ClassifierSetting> cartesianGrid(vector<double> Cs, vector<double> nus){
        
        vector<ClassifierSetting> result;
        
        for(int i = 0; i < Cs.size(); i++){
            
            for(int j = 0; j < nus.size(); j++){
                
                result.push_back(ClassifierSetting(Cs[i], nus[j]));
                
            }
            
            
        }
        
        return result;
        
    }
    
    
    
    
    
    // heavily adapted version of autoTrain in opencv_source/modules/ml/src/svm.cpp
    Ptr<SVM> gridSearchLinearNuSVR(vector<vector<float> > features, vector<float> responses, int kFold, vector<ClassifierSetting> classifierSettings, vector<float> scores, HOGDescriptor hogDescriptor, vector<Mat> images, int verbosity)
    {
        cout << "gslnusvr enter " << endl;
        
        CV_Assert( kFold >= 2 );
        
        Ptr<SVM> svm = SVM::create();
        
        svm->setKernel(SVM::LINEAR);
        svm->setType(SVM::NU_SVR);
        svm->setTermCriteria(TermCriteria(TermCriteria::MAX_ITER, 1000, 1e-3));
        
        
        int nSamples;
        int nFeatures;
        
        if(features.size() == 0){
            
            throw runtime_error("Have 0 samples");
            
        }else if(features[0].size() == 0){
            
            throw runtime_error("Have 0 features");
            
        }else{
            
            nSamples = features.size();
            nFeatures = features[0].size();
        }
        
        
        
        
        
        
        mt19937 rng;
        rng.seed(static_cast<unsigned int>(std::time(0)));
        
        
        uniform_int_distribution<> randomCrossValidationBin(0, kFold-1);
        
        vector<int> binAssignmentOfSamples;
        vector<vector<int> > binsAndTheirSamples(kFold, vector<int>());
        
        
        int minPositiveSamples = INT_MAX;
        int minNegativeSamples = INT_MAX;
        
        do{
            
            cout << "bin assignment: " << endl;
            
            for(int i = 0; i < nSamples; i++){
                
                int binOfCurrentSample = randomCrossValidationBin(rng);
                
                binAssignmentOfSamples.push_back(binOfCurrentSample);
                binsAndTheirSamples[binOfCurrentSample].push_back(i);
                
            }
            
            
            for(int i = 0; i < binsAndTheirSamples.size(); i++){
                
                cout << "Number of samples in bin " << i << ": " << binsAndTheirSamples[i].size() << endl;
                
                int nPositiveSamples = 0;
                int nNegativeSamples = 0;
                
                for(int j = 0; j < binsAndTheirSamples[i].size(); j++){
                    
                    if(responses[binsAndTheirSamples[i][j]] == 1.f){
                        
                        
                        nPositiveSamples++;
                        
                    }else{
                        nNegativeSamples++;
                    }
                    
                }
                
                cout << "Contains " << nPositiveSamples << " positive samples and " << nNegativeSamples << " negative samples " << endl;
                
                minPositiveSamples = min(nPositiveSamples, minPositiveSamples);
                minNegativeSamples = min(nNegativeSamples, minNegativeSamples);
                
            }
            
            
            
        }while(minPositiveSamples == 0 || minNegativeSamples == 0);
        
        
        double minErrorL1 = DBL_MAX;
        double minRatioOfMisclassifications = DBL_MAX;
        int minNMisclassifications = INT_MAX;
        
        ClassifierSetting bestClassifierSetting;
        
        
        
        
        
        for(int m = 0; m < classifierSettings.size(); m++){
            
            ClassifierSetting classifierSetting = classifierSettings[m];
            
            if(verbosity){
                cout << "Parameter combination " << m+1 << " of " <<  classifierSettings.size() << " C: " << classifierSetting.C << ", nu: " << classifierSetting.nu << endl;
            }
            
            svm->setC(classifierSetting.C);
            svm->setNu(classifierSetting.nu);
            
            
            
            double errorL1 = 0;
            int nMisclassifications = 0;
            int nClassificationsInTestSet = 0;
            
            for(int k = 0; k < kFold; k++){
                
                if(verbosity >= 2){
                    cout << "Doing cross validation step " << k+1 << " of " << kFold << endl;
                }
                
                vector<int> trainingSampleIndexes;
                vector<int> testSampleIndexes;
                
                
                for(int i = 0; i < kFold; i++){
                    
                    for(int j = 0; j < binsAndTheirSamples[i].size(); j++){
                        
                        if(i != k){
                            
                            trainingSampleIndexes.push_back(binsAndTheirSamples[i][j]);
                            
                        }else{
                            
                            testSampleIndexes.push_back(binsAndTheirSamples[i][j]);
                            
                        }
                        
                    }
                }
                
                
                if(verbosity >= 2){
                    cout << "Indices copied" << endl;
                }
                /*
                 cout << "using " << trainingSampleIndexes.size() << " training samples" << endl;
                cout << "using " << testSampleIndexes.size() << " test samples" << endl;
                */
                /*
                 cout << "training samples: " << endl;
                 for(int i = 0; i < trainingSampleIndexes.size(); i++){
                 
                 cout << trainingSampleIndexes[i] << endl;
                 }
                 
                 
                 cout << "test samples: " << endl;
                 for(int i = 0; i < testSampleIndexes.size(); i++){
                 
                 cout << testSampleIndexes[i] << endl;
                 }
                 
                 */
                
                /*
                 Mat tempTrainingSamples(trainingSampleIndexes.size(), nFeatures, CV_32F);
                 Mat tempTrainingResponses(trainingSampleIndexes.size(), 1, CV_32F);
                 
                 Mat tempTestSamples(testSampleIndexes.size(), nFeatures, CV_32F);
                 Mat tempTestResponses(testSampleIndexes.size(), 1, CV_32F);
                 */
                
                vector<vector<float> > currentTrainingSamples;
                vector<float> currentTrainingResponses;
                vector<vector<float> > currentTestSamples;
                vector<float> currentTestResponses;
                
                for(int i = 0; i < trainingSampleIndexes.size(); i++){
                    
                    currentTrainingSamples.push_back(features[trainingSampleIndexes[i]]);
                    currentTrainingResponses.push_back(responses[trainingSampleIndexes[i]]);
                    
                }
                
                
                for(int i = 0; i < testSampleIndexes.size(); i++){
                    
                    currentTestSamples.push_back(features[testSampleIndexes[i]]);
                    currentTestResponses.push_back(responses[testSampleIndexes[i]]);
                    
                }
                
                
                Mat currentTrainingSamplesMat = generateMat(currentTrainingSamples);
                Mat currentTrainingResponseMat = Mat(currentTrainingResponses).clone();
                Mat currentTestSamplesMat = generateMat(currentTestSamples);
                Mat currentTestResponseMat = Mat(currentTestResponses).clone();
                
                if(verbosity >= 2){
                    cout << "Data copying done" << endl;
                }
                
                if(verbosity >= 4){
                    
                    cout << currentTrainingSamplesMat << endl;
                    cout << currentTrainingResponseMat << endl;
                    cout << currentTestSamplesMat << endl;
                    cout << currentTestResponseMat << endl;
                    
                }
                /*
                cout << currentTrainingSamplesMat.size() << endl;
                cout << currentTrainingResponseMat.size() << endl;
                cout << currentTestSamplesMat.size() << endl;
                cout << currentTestResponseMat.size() << endl;
                */
                svm->train(currentTrainingSamplesMat, ROW_SAMPLE, currentTrainingResponseMat);
                
                if(verbosity >= 2){
                    cout << "Training done" << endl;
                }
                
                
                Mat predictedTestResponses;
                
                
                svm->predict(currentTestSamplesMat, predictedTestResponses, 0);
                
                if(verbosity >= 2){
                    cout << "Prediction done" << endl;
                }
                
                /*
                cout << "prediction format" << endl;
                cout << predictedTestResponses.size() << endl;
                */
                for(int i = 0; i < testSampleIndexes.size(); i++){
                    
                    float groundTruth = currentTestResponses[i];
                    float predictedValue = predictedTestResponses.at<float>(i);
                    
                    int predictedClass;
                    
                    if(predictedValue > 0){
                        predictedClass = 1;
                    }else{
                        predictedClass = -1;
                    }
                    
                    assert((int(groundTruth) == 1 || int(groundTruth) == -1));
                    
                    if(predictedClass != int(groundTruth)){
                        /*
                         cout << "misclassification: " << endl;
                         cout << "ground truth: " << groundTruth << endl;
                         cout << "predicted value: " << predictedValue << endl;
                         */
                        /*
                         imshow("misclaisified: ", images[testSampleIndexes[i]]);
                         waitKey(0);
                         */
                    }
                    
                    errorL1 += fabs(groundTruth - predictedValue);
                    nMisclassifications += (predictedClass != int(groundTruth));
                    nClassificationsInTestSet++;
                }
                
                if(verbosity >= 2){
                    cout << "error calculation done" << endl;
                }
            }
            
            double ratioOfMisclassifications = double(nMisclassifications)/(nSamples);
            
            
            if(verbosity){
                cout << "L1 error: " << errorL1 << endl;
                cout << "number of misclassifications: " << nMisclassifications << " of " << nClassificationsInTestSet << " samples" << endl;
            }
            
            if(errorL1 < minErrorL1)
            //if(nMisclassifications < minNMisclassifications || (nMisclassifications == minNMisclassifications && errorL1 < minErrorL1))
            {
                minNMisclassifications = nMisclassifications;
                minErrorL1 = errorL1;
                bestClassifierSetting = classifierSetting;
                if(verbosity){
                    cout << "------------ ^ new best parameters ^ ------------" << endl;
                }
            }
            
            
            
            
            
            
        }
        
        // Retrain using all the data and best found classifier:
        svm->setC(bestClassifierSetting.C);
        svm->setNu(bestClassifierSetting.nu);
        svm->train(generateMat(features), ROW_SAMPLE, Mat(responses).clone());
        
        return svm;
        
    }
    
    
    
    Ptr<SVM> gridSearchLinearNuSVR(vector<vector<float> > features, vector<float> responses, int kFold, vector<ClassifierSetting> classifierSettings, HOGDescriptor hogDescriptor, vector<Mat> images, int verbosity){
        
        vector<float> dummyScores;
        
        return gridSearchLinearNuSVR(features, responses, kFold, classifierSettings, dummyScores, hogDescriptor, images, verbosity);
        
    }
    
}