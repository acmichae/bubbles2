#include <iostream>

#include <boost/chrono.hpp>

#include <opencv2/opencv.hpp>

#include "../Utilities.hpp"

//#define DEBUG_MODE


using std::cout;
using std::endl;
using std::vector;
using std::to_string;

using std::chrono::steady_clock;
using std::chrono::microseconds;
using std::chrono::nanoseconds;
using std::chrono::duration_cast;

using namespace cv;

using namespace NonMaximumSuppression;


bool firstCircleIsLeft(Vec3f left, Vec3f right){
    
    return left[0] < left[1];
}

Rect softCropRoi(Size image, Rect roi){
    
    return Rect(Point(0, 0), image) & roi;
    
}


inline vector<Vec3f> recognizeBubbles(Mat frame){
    
    
    Mat processedFrame;
    
    cvtColor(frame, processedFrame, CV_BGR2GRAY);
    
    
    vector<Vec3f> circles;
    circles.reserve(20);
    
    /// Apply the Hough Transform to find the circles
    
    int dp = 1;                                                                                                              ;
    
    int canny_threshold = 100;
    int acc_threshold = 30;
    
    int min_radius = 20;
    int max_radius = 40;
    
    int min_dist = 2*min_radius+10;
    
    HoughCircles(processedFrame, circles, CV_HOUGH_GRADIENT, dp, min_dist, canny_threshold, acc_threshold, min_radius, max_radius);
    
    return circles;
    
}



int main(){
    
    int verbosity = 2;
    
    VideoCapture capture("/Users/michael/Documents/ETH/Master/SemesterProject/bubbles2/data/testVideos/5cmStraightA02__001.avi");
    
    Size frameSize;
    frameSize.width = capture.get(CAP_PROP_FRAME_WIDTH);
    frameSize.height = capture.get(CAP_PROP_FRAME_HEIGHT);
    
    int nFrames = capture.get(CAP_PROP_FRAME_COUNT);
    
    
    Rect roi(500, 95, 300, 100);
    
    
    Mat currentFrame;
    
    
    vector<Vec3f> previousBubbles;
    vector<int> previousUniqueBubbleIdentifiers;
    
    
    FileStorage fs("/Users/michael/Documents/ETH/Master/SemesterProject/bubbles2/data/dropletSorting/dataSet13/hogDescriptor.yml", FileStorage::READ);
    FileNode fn(fs["hogDescriptor"]);
    
    HOGDescriptor hogDescriptor;
    hogDescriptor.read(fn);
    
    
    
    int uniqueBubbleIdentifier = 0;
    
    vector<Mat> frames;
    
    for(int i = 0; i < nFrames; i++){
        
        capture >> currentFrame;
        
        if(currentFrame.empty()){
            
            cout << "frame empty, continue" << endl;
            continue;
        }
        
        
        frames.push_back(currentFrame.clone());
        
    }
    
    auto startAll = steady_clock::now();
    
    vector<int> currentUniqueBubbleIdentifiers(100);
    
    vector<Rect> foundRects(100);
    vector<Point> foundLocationsPoint(100);
    vector<double> weights(100);
    
    for(int i = 0; i < frames.size(); i++){
        
        
        
        
        auto start = steady_clock::now();
        
        
        
        currentFrame = frames[i];
        
        
        
#ifdef DEBUG_MODE
        
        Mat display = currentFrame.clone();
        
#endif
        
        
        auto preRecognizeBubbles = steady_clock::now();
        
        
        vector<Vec3f> currentBubbles = recognizeBubbles(currentFrame(roi));
        currentUniqueBubbleIdentifiers.resize(0);
        
        
        auto postRecognizeBubbles = steady_clock::now();
        
        
        
        std::sort(currentBubbles.begin(), currentBubbles.end(), firstCircleIsLeft);
        
        
        
        auto preMatchingBubbles = steady_clock::now();
        
        for(int j = 0; j < currentBubbles.size(); j++){
            
            
            
            int previousX = currentBubbles[j][0] - 25;
            int slack = 10;
            
            int matchedInPreviousBubbles = -1;
            
            for(int k = 0; k < previousBubbles.size(); k++){
                
                if(previousBubbles[k][0] >= previousX - slack && previousBubbles[k][0] <= previousX + slack){
                    
                    matchedInPreviousBubbles = k;
                    break;
                }
                
            }
            
            if(matchedInPreviousBubbles != -1){
                
                currentUniqueBubbleIdentifiers.push_back(previousUniqueBubbleIdentifiers[matchedInPreviousBubbles]);
                
            }else{
                
                currentUniqueBubbleIdentifiers.push_back(uniqueBubbleIdentifier++);
                
            }
            
            
            
        }
        
        
        auto postMatchingBubbles = steady_clock::now();
        
        
        
        
        
        
        auto preRecognizingCells = steady_clock::now();
        
        
        for(int j = 0; j < currentBubbles.size(); j++){
            
            
            auto prePrep = steady_clock::now();
            Vec3f currentBubble = currentBubbles[j];
            
            Point center(currentBubble[0], currentBubble[1]);
            int radius = currentBubble[2];
            
            Point topRight = center - Point(radius+5, radius+5);
            
            
            Rect bubbleRoi = Rect(topRight, Size(2*radius+10, 2*radius+10));
            
            
            
            
            Mat bubbleRoied = currentFrame(roi)(softCropRoi(roi.size(), bubbleRoi));
            
            
            auto postPrep = steady_clock::now();
            
            
            
            foundLocationsPoint.resize(0);
            weights.resize(0);
            
            
            auto preCompute = steady_clock::now();
            
            hogDescriptor.detect(bubbleRoied, foundLocationsPoint, weights);
            
            auto postCompute = steady_clock::now();
            
            
            foundRects.resize(0);
            
            for(int i = 0; i < foundLocationsPoint.size(); i++){
                
                foundRects.push_back(Rect(foundLocationsPoint[i], Size(20, 20)));
                
            }
            
            
            
            
            auto prePruningCells = steady_clock::now();
            
            vector<Rect> prunedRects = pruneRects(foundRects, weights, 0.4);
            
            
            auto postPruningCells = steady_clock::now();
            
#ifdef DEBUG_MODE
            
            if(verbosity >= 2){
                
                circle(display(roi), center, radius, Scalar(255, 255, 0), 1, LINE_AA);
                
                putText(display(roi), to_string(currentUniqueBubbleIdentifiers[j]), bubbleRoi.tl() + Point(30, 10), 1, 1, Scalar(0, 0, 0));
                putText(display(roi), to_string(prunedRects.size()), bubbleRoi.br() - Point(30, 0), 1, 1, Scalar(0, 0, 0));
                
                for(int i = 0; i < prunedRects.size(); i++){
                    
                    Mat temp = display(roi)(softCropRoi(roi.size(), bubbleRoi));
                    
                    rectangle(temp, prunedRects[i], Scalar(0, 255, 0), 1, LINE_AA);
                    
                }
                
            }
            
            
            int hogComputation = duration_cast<microseconds>(postCompute - preCompute).count();
            cout << "hog detection took " << hogComputation << " microseconds" << endl;
            
            int pruningCells = duration_cast<microseconds>(postPruningCells - prePruningCells).count();
            cout << "pruning cells took " << pruningCells << " microseconds" << endl;
#endif
        }
        
        auto postRecognizingCells = steady_clock::now();
        
        
        
        previousBubbles = currentBubbles;
        previousUniqueBubbleIdentifiers = currentUniqueBubbleIdentifiers;
        
        
        
        auto end = steady_clock::now();
        
#ifdef DEBUG_MODE
        
        cout << "Detected " << currentBubbles.size() << " bubbles" << endl;
        
        int recognizeBubbles = duration_cast<microseconds>(postRecognizeBubbles - preRecognizeBubbles).count();
        cout << "recognizing bubbles took " << recognizeBubbles << " microseconds" << endl;
        
        int matchingBubbles = duration_cast<microseconds>(postMatchingBubbles - preMatchingBubbles).count();
        cout << "matching bubbles took " << matchingBubbles << " microseconds" << endl;
        
        int recognizingCells = duration_cast<microseconds>(postRecognizingCells - preRecognizingCells).count();
        cout << "recognizing cells took " << recognizingCells << " microseconds" << endl;
        
        int totalFrame = duration_cast<microseconds>(end - start).count();
        cout << "one frame took " << totalFrame << " microseconds (" << double(totalFrame)/1000 << " ms)" << endl;
        cout << endl;
        
        if(verbosity >= 2){
            
            rectangle(display, roi, Scalar(0, 0, 0), 1, LINE_AA);
            
            imshow("", display);
            waitKey();
        }
#endif
        
        
        
        
        
    }
    
    
    auto endAll = steady_clock::now();
    
    int totalAll = duration_cast<microseconds>(endAll - startAll).count();
    
    cout << "one frame took on average " << totalAll/frames.size() << " microseconds (" << double(totalAll)/frames.size()/1000 << " ms)" << endl;
    
}

