cmake_minimum_required(VERSION 3.1)

include(../../cmake/GlobalStuff.cmake)


project(DropletSorter)

add_subdirectory(TestDropletSorter)
add_subdirectory(Tools)
