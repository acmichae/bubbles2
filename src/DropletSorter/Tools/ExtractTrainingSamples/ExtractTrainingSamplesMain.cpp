#include <iostream>
#include <sstream>

#include <exception>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/random.hpp>

#include <opencv2/opencv.hpp>

#include <cassert>
#include <iomanip>
#include <ctime>

#include "../../Utilities.hpp"



using std::cout;
using std::cerr;
using std::endl;
using std::setfill;
using std::setw;
using std::string;
using std::stringstream;
using std::ostream;
using std::vector;
using std::pair;
using std::map;
using std::exception;
using std::runtime_error;
using std::to_string;
using std::flush;

using std::chrono::system_clock;
using std::chrono::steady_clock;
using std::ctime;
using std::chrono::hours;
using std::chrono::minutes;
using std::chrono::seconds;
using std::chrono::duration_cast;

using boost::property_tree::ptree;
using boost::property_tree::read_json;

using boost::filesystem::path;

using boost::random::uniform_int_distribution;
using boost::random::discrete_distribution;
using boost::random::mt19937;

namespace po = boost::program_options;

using namespace cv;


int versionMajor = 0;
int versionMinor = 1;


struct extractTrainingSamplesCommandLineArguments{
    
    int sampleWidth;
    int sampleHeight;
    path dataSetRoot;
    int nNegativeSamples;
    int verbosity;
    bool dryRun;
    
    void write(FileStorage& fs) const{
        fs << "{" << "sampleWidth" << sampleWidth << "sampleHeight" << sampleHeight << "dataSetRoot" << dataSetRoot.string() << "nNegativeSamples" << nNegativeSamples << "verbosity" << verbosity << "dryRun" << dryRun << "}";
    }
    
    
    void read(const FileNode& node){
        
        sampleWidth = (int)node["sampleWidth"];
        sampleHeight = (int)node["sampleHeight"];
        dataSetRoot = path((string)node["dataSetRoot"]);
        nNegativeSamples = (int)node["nNegativeSamples"];
        verbosity = (int)node["verbosity"];
        dryRun = (bool)(int)node["dryRun"];
    }
    
};

void write(FileStorage& fs, const std::string&, const extractTrainingSamplesCommandLineArguments& x)
{
    x.write(fs);
}



extractTrainingSamplesCommandLineArguments parseCommandLine(int argc, char **argv){
    
    po::options_description desc("Usage");
    
    desc.add_options()
    ("version", "print version and exit")
    ("help", "produces this help message")
    ("sampleWidth,w", po::value<int>()->default_value(16), "sample width (px)")
    ("sampleHeight,h", po::value<int>()->default_value(16), "sample height (px)")
    ("dataSetRoot,d", po::value<string>()->default_value("./"), "path to the data set (the one containing extractedFrames and annotation.json)")
    ("nNegativeSamples,n", po::value<int>(), "number of negative samples to be generated, 20x found positive samples if not specified")
    ("dryRun", "just play pretend, don't generate any output files")
    ("verbosity", po::value<int>()->default_value(0), "1: print all messages\n2: use any key to move to the next picture")
    ;
    
    po::positional_options_description p;
    p.add("dataSetRoot", -1);
    
    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    
    if(argc == 1 || vm.count("help")) {
        cout << desc << endl;
        exit(EXIT_SUCCESS);
    }
    
    if(argc == 2 && vm.count("version")){
        cout << "Version: " << versionMajor << "." << setfill('0') << setw(3) << versionMinor << endl;
        exit(EXIT_SUCCESS);
    }
    
    
    
    po::notify(vm);
    
    extractTrainingSamplesCommandLineArguments result;
    
    result.sampleWidth = vm["sampleWidth"].as<int>();
    result.sampleHeight = vm["sampleHeight"].as<int>();
    result.dataSetRoot = path(vm["dataSetRoot"].as<string>());
    if(vm.count("nNegativeSamples"))
        result.nNegativeSamples = vm["nNegativeSamples"].as<int>();
    else{
        result.nNegativeSamples = -1;         // find as many as we found positiveSamples
    }
    
    result.verbosity = vm["verbosity"].as<int>();
    result.dryRun = vm.count("dryRun");
    
    
    if(result.verbosity){
        
        cout << "Command line arguments:" << endl;
        
        cout << "sampleWidth: " << result.sampleWidth << endl;
        cout << "sampleHeight: " << result.sampleHeight << endl;
        cout << "dataSetRoot: " << result.dataSetRoot << endl;
        cout << "nNegativeSamples: " << result.nNegativeSamples << endl;
        cout << "dryRun: " << result.dryRun << endl;
        cout << "verbosity: " << result.verbosity << endl;
    }
    
    return result;
    
}



class AnnotatedFrame{
    
public:
    string path;
    vector<Point> pointAnnotations;
    vector<Rect> rectAnnotations;
};


vector<AnnotatedFrame> readJsonSlothAnnotationFile(path annotationFile, bool verbosity = false){
    
    
    ptree pt;
    read_json(annotationFile.string(), pt);
    
    vector<AnnotatedFrame> result;
    
    BOOST_FOREACH(ptree::value_type v1, pt){
        
        AnnotatedFrame ai;
        
        assert(v1.second.get_child("class").get_value<string>() == "image");
        
        string framePath = v1.second.get_child("filename").get_value<string>();
        
        ai.path = framePath;
        
        
        BOOST_FOREACH(ptree::value_type &v2, v1.second.get_child("annotations")){
            
            if(v2.second.get_child("class").get_value<string>() == "point"){
                
                double x = v2.second.get_child("x").get_value<double>();
                double y = v2.second.get_child("y").get_value<double>();
                
                ai.pointAnnotations.push_back(Point(x, y));
                
            }else if(v2.second.get_child("class").get_value<string>() == "rect"){
                
                double x = v2.second.get_child("x").get_value<double>();
                double y = v2.second.get_child("y").get_value<double>();
                double width = v2.second.get_child("width").get_value<double>();
                double height = v2.second.get_child("height").get_value<double>();
                
                ai.rectAnnotations.push_back(Rect(x, y, width, height));
                
            }else{
                
                if(verbosity)
                    cout << "Found annotation of type " << v2.second.get_child("class").get_value<string>() << ", skipping it" << endl;
                
            }
            
            
            
        }
        
        result.push_back(ai);
        
    }
    
    if(verbosity){
        cout << "Annotation file sucessfully imported" << endl;
    }
    
    return result;
    
}


bool outOfBounds(Point p, Size s){
    
    return p.x < 0 || p.y < 0 || p.x > s.width || p.y > s.height;
    
}

bool outOfBounds(Rect r, Size s){
    
    return outOfBounds(r.br(), s) || outOfBounds(r.tl(), s);
    
}


int main(int argc, char **argv){
    
    try{
        
        auto start = steady_clock::now();
        
        
        auto commandLineArguments = parseCommandLine(argc, argv);
        
        path annotationFile = commandLineArguments.dataSetRoot / "annotations.json";
        
        auto annotatedFrames = readJsonSlothAnnotationFile(annotationFile, commandLineArguments.verbosity);
        
        path dataSetRoot = annotationFile.branch_path();
        
        path outputFolder = commandLineArguments.dataSetRoot / "extractedTrainingSamples";
        
        path positiveSamples = outputFolder / "positiveSamples";
        
        string sampleSuffix = "jpg";
        string samplePrefix = "svm";
        
        
        if(!is_directory(positiveSamples)){
            
            if(!create_directories(positiveSamples)){
                
                throw runtime_error("Could not create " + positiveSamples.string());
                
            }else{
                
                if(commandLineArguments.verbosity)
                    cout << "Created " << positiveSamples << endl;
                
            }
        }else{
            
            if(commandLineArguments.verbosity)
                cout << "Output directory " << positiveSamples << " found" << endl;
            
        }
        
        
        path negativeSamples = outputFolder / "negativeSamples";
        
        if(!is_directory(negativeSamples)){
            
            if(!create_directories(negativeSamples)){
                
                throw runtime_error("Could not create " + negativeSamples.string());
                
            }else{
                
                if(commandLineArguments.verbosity)
                    cout << "Created " << negativeSamples << endl;
                
            }
        }else{
            
            if(commandLineArguments.verbosity)
                cout << "Output directory " << negativeSamples << " found" << endl;
            
        }
        
        
        int positiveSamplesCounter = 0;
        int successfullpositiveSamplesCounter = 0;
        
        
        
        // positive samples
        
        for(int i = 0; i < annotatedFrames.size(); i++){
            
            path currentFramePath = dataSetRoot / annotatedFrames[i].path;
            
            if(commandLineArguments.verbosity)
                cout << "Using frame: " << currentFramePath.string() << endl;
            
            Mat frame = imread(currentFramePath.string());
            Mat display = frame.clone();
            
            
            for(int j = 0; j < annotatedFrames[i].pointAnnotations.size(); j++){
                
                Point centerOfAnnotation = annotatedFrames[i].pointAnnotations[j] - Point(commandLineArguments.sampleWidth/2, commandLineArguments.sampleHeight/2);
                
                Rect sampleRect(centerOfAnnotation, Size(commandLineArguments.sampleWidth, commandLineArguments.sampleHeight));
                
                if(outOfBounds(sampleRect, frame.size())){
                    
                    if(commandLineArguments.verbosity)
                        cout << "Out of bounds, don't use this annotation" << endl;
                    
                    continue;
                }
                
                rectangle(display, sampleRect, Scalar(0, 255, 0), 1, LINE_AA);
                
                string filename = samplePrefix + to_string(positiveSamplesCounter++) + "." + sampleSuffix;
                
                path output = positiveSamples / filename;
                
                if(!commandLineArguments.dryRun){
                    
                    Mat sample = frame(sampleRect);
                    
                    bool successfulWrite = imwrite(output.string(), sample);
                    
                    if(successfulWrite){
                        
                        successfullpositiveSamplesCounter++;
                        if(commandLineArguments.verbosity)
                            cout << "Written to " << output.string() << endl;
                        
                    }else{
                        if(commandLineArguments.verbosity)
                            cerr << "Could not write to " << output.string() << endl;
                    }
                    
                    
                    
                }
                
                
                if(commandLineArguments.verbosity){
                    
                    cout << centerOfAnnotation << endl;
                    
                }else{
                    
                    cout << "Positive samples: " << ceil(i*100.0/annotatedFrames.size()) << " % done" << "\r" << flush;
                }
                
                
            }
            
            if(commandLineArguments.verbosity || commandLineArguments.dryRun){
                imshow("Current Frame", display);
                if(commandLineArguments.verbosity >= 2){
                    waitKey();
                }else{
                    waitKey(1);
                }
            }
            
            
            
        }
        
        
        cout << "Positive samples: 100 % done" << endl;
        cout << "Generated " << successfullpositiveSamplesCounter << " positive samples" << endl;
        
        if(commandLineArguments.nNegativeSamples == -1){
            commandLineArguments.nNegativeSamples = successfullpositiveSamplesCounter*20;
        }
        
        if(commandLineArguments.verbosity)
            cout << "Trying to find " << commandLineArguments.nNegativeSamples << " negative samples" << endl;
        
        
        
        
        
        
        
        
        // negative samples
        
        int negativeSamplesCounter = 0;
        
        vector<double> negativeRegionAreas;
        vector<pair<int, int> > negativeRegionIndices; // annotatedFrames[i].rectAnnotations[j], -> first = i, second = j
        
        for(int i = 0; i < annotatedFrames.size(); i++){
            
            for(int j = 0; j < annotatedFrames[i].rectAnnotations.size(); j++){
                
                negativeRegionAreas.push_back(annotatedFrames[i].rectAnnotations[j].area());
                negativeRegionIndices.push_back(pair<int, int>(i, j));
                
            }
            
        }
        
        
        
        
        mt19937 rng;
        
        int successfulNegativeSamplesCounter = 0;
        
        while(successfulNegativeSamplesCounter < commandLineArguments.nNegativeSamples){
            
            discrete_distribution<> randomNegativeRegion(negativeRegionAreas);
            
            int k = randomNegativeRegion(rng);
            
            int i = negativeRegionIndices[k].first;
            int j = negativeRegionIndices[k].second;
            
            
            path currentFramePath = dataSetRoot / annotatedFrames[i].path;
            
            if(commandLineArguments.verbosity)
                cout << "Using frame: " << currentFramePath.string() << endl;
            
            Mat frame = imread(currentFramePath.string());
            Mat display = frame.clone();
            
            Rect negativeRegion = annotatedFrames[i].rectAnnotations[j];
            
            uniform_int_distribution<> randomX(0, negativeRegion.width - commandLineArguments.sampleWidth);
            uniform_int_distribution<> randomY(0, negativeRegion.height - commandLineArguments.sampleHeight);
            
            
            
            rectangle(display, negativeRegion, Scalar(0, 255, 255), 1, LINE_AA);
            
            
            Point topLeft(randomX(rng), randomY(rng));
            Point bottomRight = topLeft + Point(commandLineArguments.sampleWidth, commandLineArguments.sampleHeight);
            
            Rect sampleRect(topLeft, bottomRight);
            
            rectangle(display, sampleRect, Scalar(0, 0, 255), 1, LINE_AA);
            
            string filename = samplePrefix + to_string(negativeSamplesCounter++) + "." + sampleSuffix;
            
            path output = negativeSamples / filename;
            
            
            if(!commandLineArguments.dryRun){
                
                Mat sample = frame(sampleRect);
                
                bool successfulWrite = imwrite(output.string(), sample);
                
                if(successfulWrite){
                    
                    successfulNegativeSamplesCounter++;
                    if(commandLineArguments.verbosity)
                        cout << "Written to " << output.string() << endl;
                    
                }else{
                    if(commandLineArguments.verbosity)
                        cerr << "Could not write to " << output.string() << endl;
                }
                
                
            }else{
                
                successfulNegativeSamplesCounter++;
                
            }
            
            
            
            
            if(commandLineArguments.verbosity){
                
                cout << sampleRect << endl;
                
            }else{
                
                cout << "Negative samples: " << ceil(successfulNegativeSamplesCounter*100.0/commandLineArguments.nNegativeSamples) << " % done" << "\r" << flush;
            }
            
            
            if(commandLineArguments.verbosity || commandLineArguments.dryRun){
                imshow("Current Frame", display);
                if(commandLineArguments.verbosity >= 2){
                    waitKey();
                }else{
                    waitKey(1);
                }
            }
            
            
        }
        
        cout << "Negative samples: 100 % done" << endl;
        cout << "Generated " << successfulNegativeSamplesCounter << " negative samples" << endl;
        
        
        
        
        auto now = system_clock::now();
        auto now_time = system_clock::to_time_t(now);
        string date = ctime(&now_time);
        
        auto end = steady_clock::now();
        string timeUsed = miscUtilities::nicelyFormatDuration(start, end);
        
        cout << "Time used: " << timeUsed << endl;
        
        path infoFile = commandLineArguments.dataSetRoot / "extractTrainingSamplesInfo.yml";
        
        
        if(!commandLineArguments.dryRun){
            
            FileStorage fs(infoFile.string(), FileStorage::WRITE);
            
            fs << "versionMajor" << versionMajor;
            fs << "versionMinor" << versionMinor;
            fs << "date" << date;
            fs << "timeUsed" << timeUsed;
            
            fs << "nPositiveSamplesGenerated" << positiveSamplesCounter;
            fs << "nNegativeSamplesGenerated" << successfulNegativeSamplesCounter;
            
            fs << "commandLineArguments" << commandLineArguments;
            
        }
        
        if(commandLineArguments.verbosity && !commandLineArguments.dryRun){
            cout << "Wrote info file to " << infoFile.string() << endl;
        }
        
        
        
        
    }catch (exception const& e){
        cerr << e.what() << endl;
    }
    
    
}