#include <iostream>

#include <exception>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

#include <opencv2/opencv.hpp>



#include <cassert>

#include "../../Utilities.hpp"


using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::stringstream;
using std::vector;
using std::exception;
using std::runtime_error;
using std::to_string;

using boost::property_tree::ptree;
using boost::property_tree::read_json;

using boost::filesystem::exists;
using boost::filesystem::path;
using boost::filesystem::directory_iterator;
using boost::filesystem::is_directory;


namespace po = boost::program_options;

using namespace cv;
using namespace cv::ml;

using namespace SVMUtilities;
using namespace NonMaximumSuppression;




struct trainClassifierCommandLineArguments{
    path dataSetRoot;
    path outputDirectory;
    Size winSize;
    Size blockSize;
    Size blockStride;
    Size cellSize;
    int nbins;
    float C;
    float nu;
    bool doCrossValidation;
    int kFold;
    float minC;
    float maxC;
    int nStepsC;
    float minNu;
    float maxNu;
    int nStepsNu;
    bool testWithImage;
    path testImage;
    bool dryRun;
    int verbosity;
};



trainClassifierCommandLineArguments parseCommandLine(int argc, char **argv){
    
    po::options_description desc("Usage");
    
    desc.add_options()
    ("help", "produces help message")
    ("dataSetRoot,d", po::value<string>()->default_value("./"), "path to the data set (the one containing extractedFrames, annotation.json and samplesForClassifierTraining")
    ("outputDirectory,o", po::value<string>()->default_value("./"), "output directory")
    ("winSize", po::value<int>(), "window size for hog descriptor (if not specified, use sample size from extractTrainingSamplesInfo.yml)")
    ("blockSize", po::value<int>(), "block size for hog descriptor")
    ("blockStride", po::value<int>(), "block stride for hog descriptor")
    ("cellSize", po::value<int>(), "cell size for hog descriptor")
    ("nbins", po::value<int>()->default_value(9), "number of bins for hog descriptor")
    ("C,c", po::value<float>()->default_value(0.001), "C value to be used for svm training in case we don't do cross validation")
    ("nu,n", po::value<float>()->default_value(0.001), "nu value to be used for svm training in case we don't do cross validation")
    ("crossValidationKFold,k", po::value<int>(), "do k-fold cross validation using parameter grid to find optimal values for C and nu (has to be >= 2)")
    ("minC", po::value<float>()->default_value(0.01), "minimal C value")
    ("maxC", po::value<float>()->default_value(3), "maximal C value")
    ("nStepsC", po::value<int>()->default_value(5), "number of steps for C value")
    ("minNu", po::value<float>()->default_value(0.01), "minimal nu value")
    ("maxNu", po::value<float>()->default_value(0.99), "maximal nu value")
    ("nStepsNu", po::value<int>()->default_value(5), "number of steps for nu value")
    ("testImage,t", po::value<string>(), "test image to assess quality of classifier")
    ("dryRun", "just play pretend")
    ("verbosity", po::value<int>()->default_value(0), "1: print all messages\n2: use any key to move to the next picture")
    ;
    
    po::positional_options_description p;
    
    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    
    if (vm.count("help") || argc == 1) {
        cout << desc << endl;
        exit(EXIT_SUCCESS);
    }
    
    
    
    po::notify(vm);
    
    trainClassifierCommandLineArguments result;
    
    result.verbosity = vm["verbosity"].as<int>();
    
    result.dataSetRoot = path(vm["dataSetRoot"].as<string>());
    //result.outputDirectory = path(vm["outputDirectory"].as<string>());
    
    if(vm.count("winSize")){
        
        result.winSize = Size(vm["winSize"].as<int>(), vm["winSize"].as<int>());
        
    }else{
        
        path infoFile = result.dataSetRoot / "extractTrainingSamplesInfo.yml";
        
        if(result.verbosity){
            
            cout << "Attempting to read winSize from " << infoFile.string() << ": " << result.winSize << endl;
        }
        
        if(!exists(infoFile)){
            throw runtime_error("File does not exists: " + infoFile.string() + "\n");
        }
        
        
        FileStorage fs(infoFile.string(), FileStorage::READ);
        
        if (!fs.isOpened())
        {
            throw string("Could not read " + infoFile.string() + "\n");
        }
        
        int sampleWidth = fs["commandLineArguments"]["sampleWidth"];
        int sampleHeight = fs["commandLineArguments"]["sampleHeight"];
        
        
        result.winSize = Size(sampleWidth, sampleHeight);
        
        if(result.verbosity){
            
            cout << "Read winSize from " << infoFile.string() << ": " << result.winSize << endl;
        }
        
        
    }
    
    
    
    if(vm.count("blockSize")){
        
        result.blockSize = Size(vm["blockSize"].as<int>(), vm["blockSize"].as<int>());
        
    }else{
        
        if(result.winSize != Size(20, 20)){
            result.blockSize = result.winSize;
        }else{
            result.blockSize = Size(12, 12);
        }
            
    }
    
    
    
    if(vm.count("blockStride")){
        
        result.blockStride = Size(vm["blockStride"].as<int>(), vm["blockStride"].as<int>());
        
    }else{
        
        if(result.winSize != Size(20, 20)){
            result.blockStride = result.winSize;
        }else{
            result.blockStride = Size(4, 4);
        }
        
    }
    
    
    if(vm.count("cellSize")){
        
        result.cellSize = Size(vm["cellSize"].as<int>(), vm["cellSize"].as<int>());
        
    }else{
        
        if(result.winSize != Size(20, 20)){
            result.cellSize = result.winSize;
        }else{
            result.cellSize = Size(4, 4);
        }
        
    }
    
    
    
    result.nbins = vm["nbins"].as<int>();
    
    
    result.C = vm["C"].as<float>();
    result.nu = vm["nu"].as<float>();
    
    
    if(vm.count("crossValidationKFold")){
        result.kFold = vm["crossValidationKFold"].as<int>();
        result.doCrossValidation = true;
    }else{
        result.kFold = 0;
        result.doCrossValidation = false;
    }
    
    result.minC = vm["minC"].as<float>();
    result.maxC = vm["maxC"].as<float>();
    result.nStepsC = vm["nStepsC"].as<int>();
    result.minNu = vm["minNu"].as<float>();
    result.maxNu = vm["maxNu"].as<float>();
    result.nStepsNu = vm["nStepsNu"].as<int>();
    
    
    if(vm.count("testImage")){
        result.testImage = path(vm["testImage"].as<string>());
        result.testWithImage = true;
    }else{
        result.testWithImage = false;
    }
    
    
    result.dryRun = vm.count("dryRun");
    
    if(result.verbosity){
        
        cout << "--------------------------------------------" << endl;
        cout << "Command line arguments:" << endl;
        cout << "--------------------------------------------" << endl;
        cout << "dataSetRoot: " << result.dataSetRoot.string() << endl;
        cout << "outputDirectory: " << result.outputDirectory.string() << endl;
        cout << "winSize: " << result.winSize << endl;
        cout << "blockSize: " << result.blockSize << endl;
        cout << "blockStride: " << result.blockStride << endl;
        cout << "cellSize: " << result.cellSize << endl;;
        cout << "nbins: " << result.nbins << endl;
        cout << "C: " << result.C << endl;
        cout << "nu: " << result.nu << endl;
        cout << "doCrossValidation: " << result.doCrossValidation << endl;
        cout << "kFold: " << result.kFold << endl;
        cout << "minC: " << result.minC << endl;
        cout << "maxC: " << result.maxC << endl;
        cout << "nStepsNu: " << result.nStepsC << endl;
        cout << "minNu: " << result.minNu << endl;
        cout << "maxNu: " << result.maxNu << endl;
        cout << "nStepsC: " << result.nStepsNu << endl;
        cout << "testImage: " << result.testImage.string() << endl;
        cout << "dryRun: " << result.dryRun << endl;
        cout << "verbosity: " << result.verbosity << endl;
        cout << "--------------------------------------------" << endl;
        
    }
    
    return result;
    
}



int main(int argc, char **argv){
    
    try{
        
        auto commandLineArguments = parseCommandLine(argc, argv);
        
        
        
        HOGDescriptor hogDescriptor(commandLineArguments.winSize, commandLineArguments.blockSize, commandLineArguments.blockStride, commandLineArguments.cellSize, commandLineArguments.nbins);
        
        
        hogDescriptor.checkDetectorSize();
        // If not correct, the program stops here
        
        size_t hogDescriptorSize = hogDescriptor.getDescriptorSize();
        
        if(commandLineArguments.verbosity){
            cout << "We get a feature vector of length " << hogDescriptorSize << endl;
        }
        
        path extractedTrainingSamples = commandLineArguments.dataSetRoot / "extractedTrainingSamples";
        
        
        
        
        
        
        path positiveSamples = extractedTrainingSamples / "positiveSamples";
        
        if(!is_directory(positiveSamples)){
            
            throw runtime_error("Could not find " + positiveSamples.string());
            
        }
        
        
        
        vector<Mat> images;
        
        
        //Mat allFeaturesMat;
        vector<float> allResponses;
        vector<vector<float> > features;
        
        cout << "Computing hog for positives samples" << endl;
        
        directory_iterator end_iter;
        for(directory_iterator itr(positiveSamples); itr != end_iter; itr++){
            
            path imagePath = itr->path();
            
            Mat image = imread(imagePath.string(), IMREAD_GRAYSCALE);
            
            if(!image.data){
                
                if(commandLineArguments.verbosity >= 3){
                    cout << "Could not read " << imagePath.string() << endl;
                    continue;
                }
                
            }else{
                
                
                vector<float> descriptors;
                //cout << "passed descriptor" << endl;
                //cout << image.size() << endl;
                //cout << imagePath.string() << endl;
                hogDescriptor.compute(image, descriptors);
                //cout << "passed compute" << endl;
                //appendVectorToFeatureMatrixForSvmTraining(allFeaturesMat, descriptors);
                
                if(image.size() != commandLineArguments.winSize){
                    
                    cout << "Found image (" << imagePath.string() << ") that has size: " << image.size() << endl;
                    cout << "skipping it" << endl;
                    continue;
                }
                
                features.push_back(descriptors);
                allResponses.push_back(1.f);
                //images.push_back(image.clone());
                
                if(commandLineArguments.verbosity >= 3){
                    cout << "Successfully read " << imagePath.string() << endl;
                    imshow("Current", image);
                    if(commandLineArguments.verbosity >= 4){
                        waitKey();
                    }else{
                        waitKey(1);
                    }
                }
                
                
            }
            
        }
        
        
        path negativeSamples = extractedTrainingSamples / "negativeSamples";
        
        
        if(!is_directory(negativeSamples)){
            
            throw runtime_error("Could not find " + negativeSamples.string());
            
        }
        
        cout << "Computing hog for negative samples" << endl;
        
        for(directory_iterator itr(negativeSamples); itr != end_iter; itr++){
            
            path imagePath = itr->path();
            
            Mat image = imread(imagePath.string(), IMREAD_GRAYSCALE);
            
            if(!image.data){
                
                if(commandLineArguments.verbosity >= 3){
                    cout << "Could not read " << imagePath.string() << endl;
                    continue;
                }
                
            }else{
                
                if(image.size() != commandLineArguments.winSize){
                    
                    cout << "Found image (" << imagePath.string() << ") that has size: " << image.size() << endl;
                    cout << "skipping it" << endl;
                    continue;
                }
                
                
                vector<float> descriptors;
                hogDescriptor.compute(image, descriptors);
                
                Mat singleFeatureVector(descriptors, true);
                
                //appendVectorToFeatureMatrixForSvmTraining(allFeaturesMat, descriptors);
                allResponses.push_back(-1.f);
                //images.push_back(image.clone());
                features.push_back(descriptors);
                
                if(commandLineArguments.verbosity >= 3){
                    cout << "Successfully read " << imagePath.string() << endl;
                    imshow("Current", image);
                    if(commandLineArguments.verbosity >= 4){
                        waitKey();
                    }else{
                        waitKey(1);
                    }
                }
                
            }
            
        }
        
        
        
        
        Mat allResponsesMat = Mat(allResponses, CV_32F).clone();
        
        
        if(commandLineArguments.verbosity){
            
            cout << "Computed " << features.size() << " feature vectors" << endl;
            cout << "Each of those contains " << features[0].size() << " features" << endl;
            
        }
        /*
        if(commandLineArguments.verbosity >= 2){
            
            cout << "All the data: " << endl;
            cout << allFeaturesMat << endl;
            cout << allResponsesMat << endl;
            
        }
        */
        
        
        
        
        
        
        Ptr<SVM> svm;
        
        if(!commandLineArguments.doCrossValidation){
            
            svm = SVM::create();
            svm->setKernel(SVM::LINEAR);
            svm->setType(SVM::NU_SVR);
            svm->setTermCriteria(TermCriteria(TermCriteria::MAX_ITER, 1000, 1e-3));
            
            svm->setC(commandLineArguments.C);
            svm->setNu(commandLineArguments.nu);
            
            svm->train(generateMat(features), ROW_SAMPLE, Mat(allResponses).clone());
            
            if(commandLineArguments.verbosity){
                
                cout << "SVM trained" << endl;
                
            }
            
            
        }else{
            

            vector<double> rangeC = linspace(commandLineArguments.minC, commandLineArguments.maxC, commandLineArguments.nStepsC);
            vector<double> rangeNu = linspace(commandLineArguments.minNu, commandLineArguments.maxNu, commandLineArguments.nStepsNu);
            
            
            vector<ClassifierSetting> classifierSettings = cartesianGrid(rangeC, rangeNu);
            
            svm = gridSearchLinearNuSVR(features, allResponses, commandLineArguments.kFold, classifierSettings, hogDescriptor, images, 1);

            if(commandLineArguments.verbosity){
                
                cout << "Best parameters (cross validation score) found through grid search: " << endl;
                cout << "C: " << svm->getC() << ", nu: " << svm->getNu() << endl;
                
            }
            
        }
        
        
        
        vector<float> svmDetector = computeSvmDetectorForHOGDescriptor(svm);
        
        hogDescriptor.setSVMDetector(svmDetector);
        
        path hogDescriptorOutput = commandLineArguments.dataSetRoot / "hogDescriptor.yml";
        
        hogDescriptor.save(hogDescriptorOutput.string());
        cout << "Wrote trained hogDescriptor to " << hogDescriptorOutput.string() << endl;
        
        
        
        if(commandLineArguments.testWithImage){
            
            
            Mat testImage = imread(commandLineArguments.testImage.string());
            
            if(!testImage.data){
                
                cerr << "Could not read " << commandLineArguments.testImage.string() << endl;
                return -1;
                
            }else{
                
                if(commandLineArguments.verbosity)
                    cout << "Successfully read " << commandLineArguments.testImage.string() << endl;
                
            }
            
            cout << testImage.size() << endl;
            cout <<  commandLineArguments.winSize << endl;
            if(testImage.size() == commandLineArguments.winSize){
                
                
                cout << "Image size matches hog winSize, we try out the svm directly" << endl;
                
                vector<float> descriptors;
                hogDescriptor.compute(testImage, descriptors);
                
                Mat featureMat = convertHogFeatureVectorToMatForSvm(descriptors);
                
                Mat predictedResponse;
                
                svm->predict(featureMat, predictedResponse);
                
                cout << "The svm says: " << endl;
                cout << predictedResponse << endl;
                
            }
            
            
            vector<Point> foundLocationPoints;
            vector<double> weights;
            hogDescriptor.detect(testImage, foundLocationPoints, weights);
            
            
            cout << "Number of objects detected (before non-maxima suppression): " << endl;
            cout << foundLocationPoints.size() << endl;
            
            Mat testImageUnpruned = testImage.clone();
            
            vector<Rect> foundRects;
            
            for(int i = 0; i < foundLocationPoints.size(); i++){
                
                rectangle(testImageUnpruned, foundLocationPoints[i], foundLocationPoints[i] + Point(20, 20), Scalar(0, 255, 0), 1, LINE_AA);
                foundRects.push_back(Rect(foundLocationPoints[i], Size(20, 20)));
                
            }
            
            imshow("Before Non Maximum Suppression", testImageUnpruned);
            
            Mat testImagePruned = testImage.clone();
            
            
            vector<Rect> prunedRects = pruneRects(foundRects, weights, 0.4);
            
            cout << "Number of objects detected (after non-maxima suppression): " << endl;
            cout << prunedRects.size() << endl;
            
            for(int i = 0; i < prunedRects.size(); i++){
                
                rectangle(testImagePruned, prunedRects[i], Scalar(0, 255, 0), 1, LINE_AA);
                
                
            }
            
            
            
            imshow("Final Output", testImagePruned);
            waitKey();
            
            
        }
        
    }catch (exception const& e){
        
        cerr << e.what() << endl;
        
    }
    
    
}