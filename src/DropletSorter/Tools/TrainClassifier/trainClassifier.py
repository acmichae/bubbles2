import numpy as np
import cv2

from sklearn.svm import SVC

import itertools

import fnmatch
import os


def appendDescriptor(featureMatrix, descriptor):

    print list(itertools.chain(descriptor))


def getSvmDetector(svm):
        
    sv = [  -9.13327575e-01, -2.49476457e+00, -2.00314426e+00,
            -1.55095363e+00, 1.73507345e+00, 7.05671668e-01,
             2.32881522e+00, 1.77993572e+00, 1.07751977e+00,
            -3.98914903e-01, 2.43779615e-01, 1.40068233e+00,
            4.37811524e-01, 2.28935957e+00, -2.96439370e-03,
            -9.07293856e-02, -2.26491594e+00, -1.58551300e+00,
            9.17634249e-01, 8.44134092e-01, 3.33892256e-01,
            -1.26736867e+00, -5.72699718e-02, -2.44746256e+00,
            -8.77930582e-01, -6.36384249e-01, 2.24489927e-01,
            -4.57220465e-01, -1.40022588e+00, -9.18457091e-01,
            -1.19927132e+00, 2.20261291e-01, -1.59389699e+00,
            1.04521923e-01, -9.51739311e-01, -1.17922831e+00 ]

    sv_total = 1;
        

    rho = svm.get_support_vector_count();

    return 43.2
        
        
        
        # CV_Assert( alpha.total() == 1 && svidx.total() == 1 && sv_total == 1 );
        
        # CV_Assert( (alpha.type() == CV_64F && alpha.at<double>(0) == 1.) ||
        #           (alpha.type() == CV_32F && alpha.at<float>(0) == 1.f) );
        # CV_Assert( sv.type() == CV_32F );
        
        # hog_detector.clear();
        
        # hog_detector.resize(sv.cols + 1);
        # memcpy(&hog_detector[0], sv.ptr(), sv.cols*sizeof(hog_detector[0]));
        # hog_detector[sv.cols] = (float)-rho;
        
        # return hog_detector;

    



# struct trainClassifierCommandLineArguments{
#     path samplesForClassifierTraining;
#     path classifier;
#     bool testWithImage;
#     path testImage;
#     bool dryRun;
#     int verbosity;
# };



# trainClassifierCommandLineArguments parseCommandLine(int argc, char **argv){
    
#     po::options_description desc("Usage");
    
#     desc.add_options()
#     ("help", "produces help message")
#     ("samplesForClassifierTraining,s", po::value<string>()->default_value("samplesForClassifierTraining"), "samplesForClassifierTraining folder containing a folder called positives and one called negatives")
#     ("classifier,c", po::value<string>()->default_value("classifiers/classifier1"), "path to where the resulting classifier parameters should be saved")
#     ("testImage,t", po::value<string>(), "test image to assess quality of classifier")
#     ("dryRun,d", "don't generate training images")
#     ("verbosity,v", po::value<int>()->default_value(0), "1: print all messages\n2: use any key to move to the next picture")
#     ;
    
#     po::positional_options_description p;
    
#     po::variables_map vm;
#     po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    
#     if (vm.count("help") || argc == 1) {
#         cout << desc << endl;
#         exit(EXIT_SUCCESS);
#     }
    
#     po::notify(vm);
    
#     trainClassifierCommandLineArguments result;
    
#     result.samplesForClassifierTraining = path(vm["samplesForClassifierTraining"].as<string>());
#     result.classifier = path(vm["classifier"].as<string>());
    
#     if(vm.count("testImage")){
#         result.testImage = path(vm["testImage"].as<string>());
#         result.testWithImage = true;
#     }else{
#         result.testWithImage = false;
#     }
    
#     result.verbosity = vm["verbosity"].as<int>();
#     result.dryRun = vm.count("dryRun");
    
#     if(result.verbosity){
        
#         cout << "Command line arguments:" << endl;
        
#         cout << "samplesForClassifierTraining: " << result.samplesForClassifierTraining.string() << endl;
#         cout << "classifier: " << result.classifier.string() << endl;
#         cout << "testImage: " << result.testImage.string() << endl;
#         cout << "dryRun: " << result.dryRun << endl;
#         cout << "verbosity: " << result.verbosity << endl;
#     }
    
#     return result;
    
# }



try:


    commandLineArguments = {'help': False, 
                        'samplesForClassifierTraining': '/Users/michael/Documents/ETH/Master/SemesterProject/bubbles2/data/dropletSorting/dataSet6/samplesForClassifierTraining',
                        'classifier': 'classifier1.yml',
                        'testWithImage': "/Users/michael/Documents/ETH/Master/SemesterProject/bubbles2/data/dropletSorting/dataSet5/extractedFrames/5cmStraightA02_93.jpg",
                        'dryRun': False,
                        'verbosity': 2};



    winSize = (16, 16)
    blockSize = (16, 16)
    blockStride = (16, 16)
    cellSize = (8, 8)
    nbins = 9
        
    hogDescriptor = cv2.HOGDescriptor(winSize, blockSize, blockStride, cellSize, nbins)


    allFeatures = np.array([])
    allLabels = np.array([])



    positiveSamplesPath = os.path.join(commandLineArguments['samplesForClassifierTraining'],  "positives")
        
    if not os.path.isdir(positiveSamplesPath):
        raise NameError("Could not find " + positiveSamplesPath)
            

    print  "Computing hog for positives samples"
        
    for fileName in os.listdir(positiveSamplesPath):

        imagePath = os.path.join(positiveSamplesPath, fileName)
        image = cv2.imread(imagePath, cv2.CV_LOAD_IMAGE_GRAYSCALE)

        if image is None:
            if commandLineArguments['verbosity']:
                print "Could not read " + imagePath
                continue
        else:
            cv2.imshow('image',image)
            descriptors = hogDescriptor.compute(image)
            
            allFeatures = np.vstack((allFeatures, np.transpose(descriptors))) if allFeatures.size else np.transpose(descriptors)
            allLabels = np.hstack((allLabels, 1)) if allLabels.size else np.array(1)



    negativeSamplesPath = os.path.join(commandLineArguments['samplesForClassifierTraining'],  "negatives")

    if not os.path.isdir(negativeSamplesPath):
        raise NameError("Could not find " + negativeSamplesPath)

    print  "Computing hog for negative samples"
        
    for fileName in os.listdir(negativeSamplesPath):

        imagePath = os.path.join(negativeSamplesPath, fileName)
        image = cv2.imread(imagePath, cv2.CV_LOAD_IMAGE_GRAYSCALE)

        if image is None:
            if commandLineArguments['verbosity']:
                print "Could not read " + imagePath
                continue
        else:
            cv2.imshow('image',image)
            descriptors = hogDescriptor.compute(image)
            
            allFeatures = np.vstack((allFeatures, np.transpose(descriptors))) if allFeatures.size else np.transpose(descriptors)
            allLabels = np.hstack((allLabels, -1)) if allLabels.size else np.array(-1)


    print "Computed " + str(allFeatures.shape[0]) + " feature vectors, each of which contains " + str(allFeatures.shape[1]) + " elements"
        
    
 
    # svm_params = dict(  kernel_type = cv2.SVM_LINEAR,
    #                     svm_type = cv2.SVM_C_SVC,
    #                     C = 0.8, 
    #                     gamma=0.2)
        
    # svm = cv2.SVM()
    # svm.train(allFeaturesArray, allLabelsArray, params=svm_params)
    # svm.save('python_classifier.yml')

    clf = SVC()
    clf.fit(allFeatures, allLabels)


    if commandLineArguments['testWithImage']: 
          

        svmDetector = getSvmDetector(svm)  
            
       
            
            
        #     hogDescriptor.setSVMDetector(svmDetector);
            
            
        #     Mat testImage = imread(commandLineArguments.testImage.string());
            
        #     if(!testImage.data){
                
        #         cerr << "Could not read " << commandLineArguments.testImage.string() << endl;
        #         return -1;
                
        #     }else{
                
        #         if(commandLineArguments.verbosity)
        #             cout << "Successfully read " << commandLineArguments.testImage.string() << endl;
                
        #     }
            
        #     // detect(const Mat& img, CV_OUT std::vector<Point>& foundLocations,
        #     // CV_OUT std::vector<double>& weights,
        #     // double hitThreshold = 0, Size winStride = Size(),
        #     // Size padding = Size(),
        #     // const std::vector<Point>& searchLocations = std::vector<Point>()) const;
            
        #     vector<Point> foundLocationsPoint;
        #     vector<double> weights;
        #     hogDescriptor.detect(testImage, foundLocationsPoint, weights);
            
            
        #     cout << "detect" << endl;
        #     cout << foundLocationsPoint.size() << endl;
            
        #     Mat testImageUnpruned = testImage.clone();
            
        #     vector<Rect> foundRects;
            
        #     for(int i = 0; i < foundLocationsPoint.size(); i++){
                
        #         rectangle(testImageUnpruned, foundLocationsPoint[i], foundLocationsPoint[i] + Point(20, 20), Scalar(0, 255, 0), 1, LINE_AA);
        #         foundRects.push_back(Rect(foundLocationsPoint[i], Size(20, 20)));
                
        #     }
            
        #     imshow("Before Non Maximum Suppression", testImageUnpruned);
            
        #     Mat testImagePruned = testImage.clone();
        #     auto prunedPoints = foundLocationsPoint;
            
        #     NonMaximumSuppression::pruneRects(foundRects, weights, 0.4);
            
            
        #     for(int i = 0; i < foundRects.size(); i++){
                
        #         rectangle(testImagePruned, foundRects[i], Scalar(0, 255, 0), 1, LINE_AA);
                
                
        #     }
            
            
            
        #     imshow("Final Output", testImagePruned);
        #     waitKey();
            
            
        # }


except NameError as e:
    print e

