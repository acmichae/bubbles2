#include <string>
#include <iostream>

#include <boost/filesystem.hpp>
#include <opencv2/opencv.hpp>


using namespace cv;
using namespace std;
using namespace boost;
using namespace boost::filesystem;



int main(int argc, char** argv){
    
    if(argc != 5){
        
        cout << "Usage: " << argv[0] << " video outputDirectory filePrefix fileSuffix" << endl;
        cout << "Example: " << argv[0] << "video.avi frames droplet jpg" << endl;
        cout << "Results in frames/droplet0.jpg, frames/droplet1.jpg and so on" << endl;
        return -1;
        
    }
    
    string videoPath = argv[1];
    string outputDirectoryPathString = argv[2];
    string filePrefix = argv[3];
    string fileSuffix = argv[4];
    
    VideoCapture cap(videoPath);
    
    if(!cap.isOpened()){
        
        cout << "Could not open " << videoPath << endl;
        
    }
    
    
    int nFrames = cap.get(CAP_PROP_FRAME_COUNT);
    
    
    path outputDirectoryPath = path(outputDirectoryPathString);
    
    if(!is_directory(outputDirectoryPath)){
        
        if(!create_directory(outputDirectoryPath)){
            
            cout << "Could not create " << outputDirectoryPathString << endl;
            return -1;
            
        }else{
            
            cout << "Created " << outputDirectoryPathString << endl;
            
        }
    }
    
    
    for(int i = 0; i < nFrames; i++){
        
        string currentFilename = filePrefix + to_string(i) + "." + fileSuffix;
        
        path currentOutputPath;
        
        currentOutputPath /= outputDirectoryPath;
        currentOutputPath /= currentFilename;
        
        Mat currentFrame;
        
        if(cap.read(currentFrame)){
            
            if(imwrite(currentOutputPath.string(), currentFrame)){
                cout << "Wrote " << currentOutputPath << endl;
            }else{
                cout << "Could not write " << currentOutputPath << endl;
            }
            
        }else{
            
            cout << "Could not read frame " << i << ", skipping it" << endl;
        }
        
        
    }
    
    return 0;
    
}


