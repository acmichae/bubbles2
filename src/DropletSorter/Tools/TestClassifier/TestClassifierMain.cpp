#include <iostream>
#include <opencv2/opencv.hpp>

#include <boost/filesystem.hpp>

#include "../../Utilities.hpp"


using std::cout;
using std::endl;
using std::vector;
using std::string;

using boost::filesystem::path;

using namespace cv;
using namespace cv::ml;

using namespace NonMaximumSuppression;


struct TestClassifierCommandLineArguments{
    
    bool useTestImage;
    path testImage;
    bool useTestVideo;
    path testVideo;
    path classifier;
    int verbosity;
    
};



int main( int argc, char** argv )
{
    
    TestClassifierCommandLineArguments commandLineArguments;
    
    commandLineArguments.verbosity = 2;
    commandLineArguments.useTestImage = false;
    //commandLineArguments.testImage;
    commandLineArguments.useTestVideo = true;
    commandLineArguments.testVideo = "/Users/michael/Documents/ETH/Master/SemesterProject/bubbles2/data/testVideos/5cmStraightA04__001.avi";
    commandLineArguments.classifier = "/Users/michael/Documents/ETH/Master/SemesterProject/bubbles2/data/dropletSorting/dataSet13/hogDescriptor.yml";
    
    
    
    FileStorage fs(commandLineArguments.classifier.string(), FileStorage::READ);
    FileNode fn(fs["hogDescriptor"]);
    
    HOGDescriptor hogDescriptor;
    hogDescriptor.read(fn);
    
    if(commandLineArguments.useTestImage){
        Mat testImage = imread(commandLineArguments.testImage.string());
        
        if(!testImage.data){
            
            cerr << "Could not read " << commandLineArguments.testImage.string() << endl;
            return -1;
            
        }else{
            
            if(commandLineArguments.verbosity)
                cout << "Successfully read " << commandLineArguments.testImage.string() << endl;
            
        }
        
        
        
        vector<Point> foundLocationsPoint;
        vector<double> weights;
        hogDescriptor.detect(testImage, foundLocationsPoint, weights);
        
        
        cout << "Number of objects detected (before non-maxima suppression): " << endl;
        cout << foundLocationsPoint.size() << endl;
        
        Mat testImageUnpruned = testImage.clone();
        
        vector<Rect> foundRects;
        
        for(int i = 0; i < foundLocationsPoint.size(); i++){
            
            rectangle(testImageUnpruned, foundLocationsPoint[i], foundLocationsPoint[i] + Point(20, 20), Scalar(0, 255, 0), 1, LINE_AA);
            foundRects.push_back(Rect(foundLocationsPoint[i], Size(20, 20)));
            
        }
        
        imshow("Before Non Maximum Suppression", testImageUnpruned);
        
        Mat testImagePruned = testImage.clone();
        
        
        vector<Rect> prunedRects = pruneRects(foundRects, weights, 0.4);
        
        cout << "Number of objects detected (after non-maxima suppression): " << endl;
        cout << prunedRects.size() << endl;
        
        for(int i = 0; i < prunedRects.size(); i++){
            
            rectangle(testImagePruned, prunedRects[i], Scalar(0, 255, 0), 1, LINE_AA);
            
            
        }
        
    }
    
    
    
    if(commandLineArguments.useTestVideo){
        
        
        VideoCapture capture(commandLineArguments.testVideo.string());
        
        Size frameSize;
        frameSize.width = capture.get(CAP_PROP_FRAME_WIDTH);
        frameSize.height = capture.get(CAP_PROP_FRAME_HEIGHT);
        
        int nFrames = capture.get(CAP_PROP_FRAME_COUNT);
        
        
        Mat currentFrame;
        Rect roi(500, 95, 100, 100);
        for(int i = 0; i < nFrames; i++){
            
            capture >> currentFrame;
            Mat currentFrameRoied = currentFrame(roi);
            
            vector<Point> foundLocationPoints;
            vector<double> weights;
            
            
            
            hogDescriptor.detect(currentFrameRoied, foundLocationPoints, weights);
            
            
            //cout << "Number of objects detected (before non-maxima suppression): " << endl;
            //cout << foundLocationPoints.size() << endl;
            
            Mat testImageUnpruned = currentFrameRoied.clone();
            
            vector<Rect> foundRects;
            
            for(int i = 0; i < foundLocationPoints.size(); i++){
                
                rectangle(testImageUnpruned, foundLocationPoints[i], foundLocationPoints[i] + Point(20, 20), Scalar(0, 255, 0), 1, LINE_AA);
                foundRects.push_back(Rect(foundLocationPoints[i], Size(20, 20)));
                
            }
            
            //imshow("Before Non Maximum Suppression", testImageUnpruned);
            
            Mat testImagePruned = currentFrameRoied.clone();
            
            
            vector<Rect> prunedRects = pruneRects(foundRects, weights, 0.4);
            
            //cout << "Number of objects detected (after non-maxima suppression): " << endl;
            //cout << prunedRects.size() << endl;
            
            for(int i = 0; i < prunedRects.size(); i++){
                
                rectangle(currentFrameRoied, prunedRects[i], Scalar(0, 255, 0), 1, LINE_AA);
                
                
            }

            
            
            int nCells = prunedRects.size();
            
            putText(currentFrameRoied, std::to_string(nCells), Point(80, 80), FONT_HERSHEY_COMPLEX, 1, Scalar(0, 0, 0), 1, LINE_AA);
            
            rectangle(currentFrame, roi, Scalar(0, 255, 255), 2, LINE_AA);
            
            imshow("currentFrame", currentFrame);
            
            if(commandLineArguments.verbosity >= 2){
                waitKey();
            }else{
                waitKey(1);
            }
        }
        
        
        
        
        
        
    }
    
    
    
    
    
    
}


