cmake_minimum_required(VERSION 3.1)

include(../../cmake/GlobalStuff.cmake)

include(ExternalProject)

set(CMAKE_VERBOSE_MAKEFILE ON)

find_package(Git)
if(GIT_FOUND)
	message(STATUS "git found: ${GIT_EXECUTABLE}")
else()
	message(ERROR "git could not be found")
endif()


### Boost

set(BOOST_BUILD_NAME "Boost_1_57_0_Darwin_Release_Static_MultiThreading")

ExternalProject_Add(boost_project
					DOWNLOAD_DIR "${PROJECT_DIR}/ext/src/cross_platform/boost" 
					URL http://garr.dl.sourceforge.net/project/boost/boost/1.57.0/boost_1_57_0.zip	
					SOURCE_DIR "${PROJECT_DIR}/ext/src/cross_platform/boost/boost_1_57_0"
					CONFIGURE_COMMAND ""
					BUILD_COMMAND cd "${PROJECT_DIR}/ext/src/cross_platform/boost/boost_1_57_0" "&&" 					# this is a bit hacky
					./bootstrap.sh "--prefix=${PROJECT_DIR}/ext/compiled/${BOOST_BUILD_NAME}" "&&"
					./b2 -j2 variant=release link=static threading=multi runtime-link=static "--build-dir=${PROJECT_DIR}/build/ext/${BOOST_BUILD_NAME}" "&&"
					./b2 install
					INSTALL_COMMAND ""
                    )





### Dlib

set(DOWNLOAD_COMMAND 	"${CMAKE_COMMAND}" -E remove_directory "${PROJECT_DIR}/ext/src/cross_platform/dlib" "&&"		# otherwise git bitches about dlib_most_recent not being empty (directory is automatically created by ExternalProject_Add)
						"${GIT_EXECUTABLE}" clone "https://github.com/davisking/dlib.git" "${PROJECT_DIR}/ext/src/cross_platform/dlib/dlib_most_recent")

ExternalProject_Add(dlib_most_recent_project
					DOWNLOAD_COMMAND ${DOWNLOAD_COMMAND}											# must not use quotes here
					ALWAYS 1
					CONFIGURE_COMMAND ""
					BUILD_COMMAND ""
					INSTALL_COMMAND ""
                    )


ExternalProject_Add(dlib_project
					DOWNLOAD_COMMAND ""
					DOWNLOAD_DIR "${PROJECT_DIR}/ext/src/cross_platform/dlib"
					DOWNLOAD_NAME dlib-18.15.tar.bz2
					URL http://garr.dl.sourceforge.net/project/dclib/dlib/v18.15/dlib-18.15.tar.bz2
					CONFIGURE_COMMAND ""
					BUILD_COMMAND ""
					INSTALL_COMMAND ""
					)





### OpenCV and opencv_contrib

set(OPENCV_BUILD_NAME OpenCV-3.0.0-rc1_gcc_Release_Static_IPP_Python2)

set(DOWNLOAD_COMMAND 	"${CMAKE_COMMAND}" -E remove_directory "${PROJECT_DIR}/ext/src/cross_platform/opencv/opencv_contrib_most_recent" "&&"		# otherwise git bitches about opencv_contrib_most_recent not being empty (directory is automatically created by ExternalProject_Add)
						"${GIT_EXECUTABLE}" clone "https://github.com/Itseez/opencv_contrib.git" "${PROJECT_DIR}/ext/src/cross_platform/opencv/opencv_contrib_most_recent")

ExternalProject_Add(opencv_contrib_project
					DOWNLOAD_COMMAND "" #${DOWNLOAD_COMMAND}											# must not use quotes here
					ALWAYS 1
					CONFIGURE_COMMAND ""
					BUILD_COMMAND ""
					INSTALL_COMMAND ""
                    )


ExternalProject_Add(opencv_project
					DOWNLOAD_DIR "${PROJECT_DIR}/ext/src/cross_platform/opencv"
					DOWNLOAD_NAME opencv-3.0.0-rc1.zip
					URL https://codeload.github.com/Itseez/opencv/zip/3.0.0-rc1
					SOURCE_DIR "${PROJECT_DIR}/ext/src/cross_platform/opencv/opencv-3.0.0-rc1"
					CMAKE_ARGS 
					"-DCMAKE_INSTALL_PREFIX=${PROJECT_DIR}/ext/compiled/${OPENCV_BUILD_NAME}"
 					-Wno-dev 
 					-DCMAKE_OSX_ARCHITECTURES=x86_64 -DCMAKE_BUILD_TYPE=Release -DBUILD_SHARED_LIBS=OFF
					-DWITH_CUDA=OFF 
					-DBUILD_TESTS=OFF 
					-DWITH_IPP=ON 
					-DWITH_TBB=OFF -DBUILD_TBB=OFF 
					-DWITH_FFMPEG=OFF -DWITH_GSTREAMER_0_10=OFF 
					#"-DOPENCV_EXTRA_MODULES_PATH=${PROJECT_DIR}/ext/src/cross_platform/opencv/opencv_contrib_most_recent"
					-DBUILD_opencv_python2=ON
					-DBUILD_PYTHON_EXAMPLES=ON 
					-BUILD_NEW_PYTHON_SUPPORT=ON
					#"-DPYTHON2_NUMPY_INCLUDE_DIRS=${PROJECT_DIR}/util/python/venv/lib/python2.7/site-packages/numpy/core/include/numpy/" 
					#"-DPYTHON2_PACKAGES_PATH=${PROJECT_DIR}/util/python/venv/lib/python2.7/site-packages" 
					#"-DPYTHON2_EXECUTABLE=${PROJECT_DIR}/util/python/venv/bin/python2.7" 
					#"-DPYTHON2_LIBRARIES=/usr/local/Cellar/python/2.7.9/Frameworks/Python.framework/Versions/2.7/lib/libpython2.7.dylib"
					BINARY_DIR "${PROJECT_DIR}/build/ext/${OPENCV_BUILD_NAME}"
					)




set(SHARK_BUILD_NAME "Shark_most_recent")

ExternalProject_Add(shark_most_recent_project
					SVN_REPOSITORY https://svn.code.sf.net/p/shark-project/code/trunk/Shark
					SOURCE_DIR "${PROJECT_DIR}/ext/src/cross_platform/shark/Shark_most_recent"
					CMAKE_ARGS 
					#-DOPT_ENABLE_ATLAS=ON 
					#-DOPT_ENABLE_OPENMP=ON
					-DBoost_NO_SYSTEM_PATHS=TRUE
					-DBOOST_INCLUDEDIR=${PROJECT_DIR}/ext/compiled/Boost_1_57_0_Darwin_Release_Static_MultiThreading/include 
					-DBOOST_LIBRARYDIR=${PROJECT_DIR}/ext/compiled/Boost_1_57_0_Darwin_Release_Static_MultiThreading/lib/
					"-DCMAKE_INSTALL_PREFIX=${PROJECT_DIR}/ext/compiled/${SHARK_BUILD_NAME}"
					BINARY_DIR "${PROJECT_DIR}/build/ext/${SHARK_BUILD_NAME}"
					)



add_executable(DependencyTest "${PROJECT_DIR}/src/TestExternals/BasicTest/BasicTestMain.cpp")

add_dependencies(DependencyTest boost_project opencv_contrib_project opencv_project dlib_project shark_most_recent_project)